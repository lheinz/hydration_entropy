import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "hydration_entropy",
    version = "2.1",
    author = "Leonard P Heinz",
    author_email = "lheinz@gwdg.de",
    description = ("Per|Mut: Spatially resolved hydration entropies from atomistic simulations."),
    license = "GPLv3+",
    url = "https://gitlab.gwdg.de/lheinz/hydration_entropy",
    packages=['hydration_entropy', 'hydration_entropy/mda_basics',
              'hydration_entropy/entropy'],
    entry_points = {
            'console_scripts': ['transformCoordinates = hydration_entropy.entropy.transformCoordinates:__main__',
                                'runMIE = hydration_entropy.entropy.MIE:__main__',
                                'entropy_analysis = hydration_entropy.entropy.analysis:__main__',
                                'interaction_list = hydration_entropy.entropy.interaction_list:__main__']
            },
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    ],
    zip_safe = False,
    include_package_data = True,
) 
