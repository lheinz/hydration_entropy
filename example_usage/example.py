import hydration_entropy.entropy as entropy
import hydration_entropy.mda_basics as mdab
import os

os.system('mkdir output')

print 'Transforming coordinates to a center-of-mass/quaternion representation.'
transform = entropy.transformCoordinates.transform('water.gro',
                                                   'water_permute_pbc.trr',
                                                   output = 'output/water_permute_pbc.npz')
transform.transform()

#%%
print 'Do the mutual information expansion.'
print 'Start with 1st order.'
mie = entropy.MIE.mie('water_permute_pbc.npz', 'rot', 1,
              k = 1,  # choose k=1 for best accuracy
              wd = 'output/.')
mie.run()

#%%
print '2nd order is next.'
mie = entropy.MIE.mie('water_permute_pbc.npz', 'rot', 2,
              cutoff = 0.5,  # cutoff reduced for demonstration purposes
              threads = 1,  # can choose number of threads, in this case, 1 is sufficient
              parts = '1/1',  # could split calculation into n parts, then execute part i with 'i/n'
              k = 1,
              wd = 'output/.')
mie.run()

#%%
print '3rd order is last.'
mie = entropy.MIE.mie('water_permute_pbc.npz', 'rot', 3,
              cutoff = 0.305,  # cutoff reduced for demonstration purposes
              threads = 1, 
              parts = '1/1',
              k = 1,
              wd = 'output/.')
mie.run()

#%%
print 'Analyze the MIE data.'
wd = 'output'

mi1 = '%s/1stOrder.dat' %wd
mi2 = '%s/2ndOrder.dat' %wd
mi3 = '%s/3rdOrder.dat' %wd
pdb_tmp = 'water_av.pdb'
pdb_out = '%s/entropies.pdb' %wd
out = '%s/entropies.txt' %wd

A = entropy.analysis.Analysis(
        mi1 = mi1, mi2 = mi2, mi3 = mi3, output = out, 
        pdb_tmp = pdb_tmp,pdb_out = pdb_out)
A.run()

#%%
caveat = \
"""
This script is meant for demonstration purposes only. In particular,
sampling is limited to only 1000 MD snapshots and all cutoff-values
are reduced. 
"""

print caveat
