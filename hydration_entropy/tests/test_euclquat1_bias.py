import numpy as np
import matplotlib.pyplot as plt
from pytools105.entropy import kNN_mix
from pytools105.entropy import kNN_trans
from pytools105.entropy import kNN_rot
from pytools105.entropy.kNN_trans import nearest_distances as nearest_distances_trans
from pytools105.entropy.kNN_rot   import nearest_distances as nearest_distances_rot

def createQuaternion2Sample(N):
    s1, h1 = kNN_mix.createUniformSample(N, width=1., maxdistance = np.pi)
    return s1, h1

S, H      = createQuaternion2Sample(500000)
#nd_trans  = nearest_distances_trans(S[:,0:3], k=2).mean()
#nd_rot    = nearest_distances_rot(S[:,3:], k=2).mean()
#scale     = nd_rot/nd_trans

s_trans   = kNN_trans.entropy(S[:,0:3], k=2)
s_rot     = kNN_rot.entropy(S[:,3:], k=2)
scale     = np.exp((s_rot-s_trans)/3.)

print 'scale = %f, s_trans = %f, s_rot = %f' %(scale, s_trans, s_rot)

Entropies = []
Entropies_tr = []
scales = np.linspace(0.01, 50, 50)
for i, scale in zip(xrange(50),scales):
    print i
    S, H = createQuaternion2Sample(500000)
    S[:,0:3] = scale * S[:,0:3]
    entropy = kNN_mix.entropy(S, k=2) - 3*np.log(scale)
    Entropies.append(entropy)    
    entropy_tr = kNN_trans.entropy(S[:,0:3], k=2) - 3*np.log(scale) \
                    + kNN_rot.entropy(S[:,3:], k=2)
    Entropies_tr.append(entropy_tr)
Entropies = (np.array(Entropies) - H)/np.abs(H)
Entropies_tr = (np.array(Entropies_tr) - H)/np.abs(H)

#%%    
plt.hist(Entropies, bins=50, density=True)
plt.hist(Entropies_tr, bins=50, density=True, alpha=0.6)
ax = plt.gca()
yy = ax.get_ylim()
plt.plot([0,0], yy, color='red')