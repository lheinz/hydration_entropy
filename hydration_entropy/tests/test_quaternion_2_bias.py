import numpy as np
import matplotlib.pyplot as plt
from pytools105.entropy import kNN_rot

def createQuaternion2Sample(N):
    s1, h1 = kNN_rot.createUniformSample(N)
    s2, h2 = kNN_rot.createUniformSample(N)
    return np.hstack([s1,s2]), h1+h2

Entropies = []
for i in xrange(5000):
    print i
    S, H = createQuaternion2Sample(5000)
    entropy = kNN_rot.entropy(S, k=2)
    Entropies.append(entropy)
Entropies = np.array(Entropies) - H

#%%    
plt.hist(Entropies, bins=50)
ax = plt.gca()
yy = ax.get_ylim()
plt.plot([0,0], yy, color='red')