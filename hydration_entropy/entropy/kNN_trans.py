#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2020  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

Non-parametric computation of entropy and mutual-information
Adapted by G Varoquaux for code created by R Brette, itself
from several papers (see in the code).
These computations rely on nearest-neighbor statistics
"""

import numpy as np
from scipy.special import gamma,psi
from scipy import ndimage
from scipy.linalg import det
from numpy import pi
import nmslib
import sys
from sklearn.neighbors import NearestNeighbors
from scipy.stats.mstats import gmean
from scipy.optimize import brentq

from .. import mda_basics as mdab
from ..mda_basics.constants import uNmkTOverHbar2,kB,hbar,nano,amu,kTJ


EPS = np.finfo(float).eps

def _reshape_(X):
    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape
    return X, n, d
              
def mutual_information_KSG(X, k=1, alpha=0.0):
    ''' Returns the mutual information in physicsl units using the KSG method
    
    Parameters
    ===========
    X     : array-like, shape (n_samples, n_features)
            The data the entropy of which is computed
    k     : int, optional
            number of nearest neighbors for density estimation
    alpha : float, optional
            threshold ratio of Vbar/V for nonuniformity correction
            if alpha=0, no correction is applied
            
    Notes
    ======
    Kraskov, Alexander, Harald Stoegbauer, and Peter Grassberger. 
    "Estimating mutual information." Physical review E 69.6 (2004): 066138.
    and:
    Gao, Shuyang, Greg Ver Steeg, and Aram Galstyan. 
    "Efficient estimation of mutual information for strongly dependent 
    variables." Artificial Intelligence and Statistics. 2015.
    '''
    
    #adjust shape if necessary
    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape
        
    #KSG so far only implemented for two dimensions
    if d != 2:
        raise ValueError('KSG-estimator is only implemented for 2 dimensions.')
    
    ## find k nearest neightbors    
    knn = NearestNeighbors(n_neighbors=k+1, #k+1, because sklearn treats 1st nearest neighbor as the point itself; 
                           metric='chebyshev', #chebyshev-metric = max-metric aka inf-metric
                           n_jobs=1 ) #use only one core so far
    knn.fit(X)
    neighbor_ndx = knn.kneighbors(X,return_distance=False)
    neighbor_ndx = neighbor_ndx[:,1:] #throw away first column, because this is only the index of the particle itself
    
    #find epsilons
    offsets = np.abs(X[neighbor_ndx].swapaxes(0,1) - X) #absolute relative offsets to from each point to each of the k neighbors
    epsilons_half = np.max(offsets, axis=0) #half epsilon tuples for each point
    
    #do nonuniformity-correction
    if alpha > 0:
        V = 4.*np.product(epsilons_half,axis=1)
        Vbar = np.zeros(n)
        for i in xrange(n):
            neighbors = X[neighbor_ndx[i]]
            #do PCA
            neighbors = neighbors - X[i] #put X[i] in the center of the rotated box
            C = np.cov(neighbors.T)
            eigenval, eigenvec = np.linalg.eig(C)
            neighbors_pca = np.dot(neighbors,eigenvec)
            #compute volume
            max_pca = np.max(np.abs(neighbors_pca),axis=0)
            Vbar[i] = 4.*np.product(max_pca)
        Vbar2V = Vbar/V
        corr = -kB/n * np.sum(np.log(Vbar2V[Vbar2V < alpha]))
    else:
        corr = 0
        
    
    #find n_x_i's - i.e. the number of marginal samples in each bin, given by [x-epsilon/2,x+epsilon/2]
    psini_mean = 0
    for i in xrange(d): #loop over all dimensions
        marginal_X = X[:,i]
        ndx = np.argsort(marginal_X)
        marginal_X = marginal_X[ndx]
        marginal_eps = epsilons_half[ndx,i]
        starts = np.searchsorted(marginal_X,marginal_X-marginal_eps, side='left')
        stops  = np.searchsorted(marginal_X,marginal_X+marginal_eps, side='right')
        ni = stops - starts
        psini_mean = psini_mean + np.mean(psi(ni))
        
    #compute final mutual information
    mi = psi(k) - (d-1.)/k + (d-1.)*psi(n) - psini_mean
    mi = kB*mi
    return mi, mi+corr, Vbar2V

def tune_alpha(k, epsilon=5e-3, tries=5e5):
    ''' Returns an optimal alpha-value for a given number of frames (N) and a k-value.
    
    Parameters
    ===========
    k     : int
            number of nearest neighbors for density estimation
    epsilon: float, optional
            probability at which non-uniformity correction should be refused for a uniform distribution
    tries : int, optional
            the number of tries to obtain the probability
            
    Notes
    ======
    Gao, Shuyang, Greg Ver Steeg, and Aram Galstyan. 
    "Efficient estimation of mutual information for strongly dependent 
    variables." Artificial Intelligence and Statistics. 2015. Supplementary material.
    '''
    tries = int(tries)
    Vbar2V = np.zeros(tries)
    axes = np.array([2.,1.])
    V = np.product(axes)
    
    for t in xrange(tries):
        
        #k points from uniform distribution
        neighbors = np.random.random((k,2)) * axes
        
        #do PCA and obtain Vbar
        neighbors = neighbors -0.5*axes #- np.mean(neighbors,axis=0) #
        C = np.cov(neighbors.T)
        eigenval, eigenvec = np.linalg.eig(C)
        neighbors_pca = np.dot(neighbors,eigenvec)
            
        #compute volume
        max_pca = np.max(np.abs(neighbors_pca),axis=0)
        Vbar2V[t] = 4.*np.product(max_pca)/V
        #max_pca = np.max(neighbors_pca,axis=0) - np.min(neighbors_pca,axis=0)
        #Vbar2V[t] = np.product(max_pca)/V
               
    alpha = np.sort(Vbar2V)[int(epsilon*tries)]   
    return alpha

__use_nmslib__ = False

def nearest_distances(X, k=1):
    '''
    X = array(N,M)
    N = number of points
    M = number of dimensions
    returns the distance to the kth nearest neighbor for every point in X
    '''
    
    if __use_nmslib__:
        index = nmslib.init(method='kdtree', space='l2', 
                            data_type = nmslib.DataType.DENSE_VECTOR,
                            dtype = nmslib.DistType.FLOAT) #, space_params= {'bucketSize': 100})
        index.addDataPointBatch(X)
        index.createIndex(print_progress=False)
        neighbors = index.knnQueryBatch(X, k=k, num_threads=1)
        return np.array([n[1][-1] for n in neighbors])        
    else:
        knn = NearestNeighbors(n_neighbors=k)
        knn.fit(X)
        d, _ = knn.kneighbors(X) # the first nearest neighbor is itself
        return d[:, -1] # returns the distance to the kth nearest neighbor


def entropy_gaussian(C):
    '''
    Entropy of a gaussian variable with covariance matrix C with kB=1.
    '''
    if np.isscalar(C): # C is the variance
        return .5*(1 + np.log(2*pi)) + .5*np.log(C)
    else:
        n = C.shape[0] # dimension
        return .5*n*(1 + np.log(2*pi)) + .5*np.log(abs(det(C)))


def entropy(X, k=1):
    ''' Returns the entropy of the X using kB = 1.
    Parameters
    ===========
    X : array-like, shape (n_samples, n_features)
        The data the entropy of which is computed
    k : int, optional
        number of nearest neighbors for density estimation
    Notes
    ======
    Kozachenko, L. F. & Leonenko, N. N. 1987 Sample estimate of entropy
    of a random vector. Probl. Inf. Transm. 23, 95-101.
    See also: Evans, D. 2008 A computationally efficient estimator for
    mutual information, Proc. R. Soc. A 464 (2093), 1203-1215.
    and:
    Kraskov A, Stogbauer H, Grassberger P. (2004). Estimating mutual
    information. Phys Rev E 69(6 Pt 2):066138.
    '''

    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape

    # Distance to kth nearest neighbor
    # k+1 because in the scope of sklearn, 1th neighbor is the point itself
    r = nearest_distances(X, k + 1).astype(np.double)
    '''
    F. Perez-Cruz, (2008). Estimation of Information Theoretic Measures
    for Continuous Random Variables. Advances in Neural Information
    Processing Systems 21 (NIPS). Vancouver (Canada), December.
    return d*mean(log(r))+log(volume_unit_ball)+log(n-1)-log(k)
    '''
    if d < 300:
        volume_unit_ball = (pi**(.5*d)) / gamma(.5*d + 1)
        return (d*np.mean(np.log(r + np.finfo(X.dtype).eps))
                + np.log(volume_unit_ball) + psi(n) - psi(k))
    else:
        log_volume_unit_ball = .5*d*np.log(np.pi) - (.5*d*np.log(.5*d) - .5*d) #take log first to avoid overflow - using stirling approximation
        return (d*np.mean(np.log(r + np.finfo(X.dtype).eps))
                + log_volume_unit_ball + psi(n) - psi(k))
        #psi(k) instead of log(k) due to correction term, see e.g. "Efficient Estimation of Mututal Information for
        #Strongly Dependent Variables" by Shuzang Gao.
                
def entropy_sk(X, k=1., verbose=False):
    ''' Returns the entropy of the X using kB=1 and a soft (gaussian) kernel.
    Parameters
    ===========
    X : array-like, shape (n_samples, n_features)
        The data the entropy of which is computed
    k : int, optional
        number of nearest neighbors for density estimation
    Notes
    ======
    Hensen, Ulf, Helmut Grubmuller, and Oliver F. Lange. 
    "Adaptive anisotropic kernels for nonparametric estimation of absolute 
    configurational entropies in high-dimensional configuration spaces."
    Physical Review E 80.1 (2009): 011913.
    '''
        
    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape
    
    #calculate sigmas
    f = lambda s: np.sum(np.exp(-(np.linalg.norm(X-X[i],axis=1)/s)**2)) - k - 1.
    #fprime = lambda s: np.sum( 2.* (np.linalg.norm(X-X[i],axis=1)/s)**2/s * np.exp(-(np.linalg.norm(X-X[i],axis=1)/s)**2))
                        
                        
    sigmas = np.ones(n)
    #sigmas2 = np.ones(n)
    lower, upper = 1e-6, 1e3
    for i in xrange(n):
        #sigmas[i] = np.abs(newton(f,1.,fprime=fprime,tol=1e-3))
        while True:
            try:
                sigmas[i] = np.abs(brentq(f,lower,upper,xtol=1e-3))
                break
            except ValueError:
                upper = upper*2.
                lower = lower/2.
                print 'Upper/lower bounds were expanded: %f, %f.' %(lower, upper)
                
        if verbose:
            if not i%100:
                print i
        
    return (d*np.mean(np.log(sigmas)) + .5*d*np.log(np.pi) + psi(n) - psi(k)), sigmas
        

def mutual_information(variables, k=1):
    '''
    Returns the mutual information (with kB=1) between any number of variables.
    Each variable is a matrix X = array(n_samples, n_features)
    where
      n = number of samples
      dx,dy = number of dimensions
    Optionally, the following keyword argument can be specified:
      k = number of nearest neighbors for density estimation
    Example: mutual_information((X, Y)), mutual_information((X, Y, Z), k=5)
    '''
    if len(variables) < 2:
        raise AttributeError(
                "Mutual information must involve at least 2 variables")
    all_vars = np.hstack(variables)
    return (sum([entropy(X, k=k) for X in variables])
            - entropy(all_vars, k=k))


def mutual_information_2d(x, y, bins=256, sigma=1, normalized=False):
    """
    Computes (normalized) mutual information (with kB=1) between two 1D variate from a
    joint histogram.
    
    Parameters
    ----------
    x : 1D array
        first variable
    y : 1D array
        second variable
    sigma: float
        sigma for Gaussian smoothing of the joint histogram
        
    Returns
    -------
    nmi: float
        the computed similariy measure
    """
    bins = (bins,bins)
        
    jh = np.histogram2d(x, y, bins=bins, normed=False)[0]

    # smooth the jh with a gaussian filter of given sigma
    ndimage.gaussian_filter(jh, sigma=sigma, mode='constant',
                                 output=jh)

    # compute marginal histograms
    jh = jh + EPS
    sh = np.sum(jh)
    jh = jh / sh
    s1 = np.sum(jh, axis=0).reshape((-1, jh.shape[0]))
    s2 = np.sum(jh, axis=1).reshape((jh.shape[1], -1))

    # Normalised Mutual Information of:
    # Studholme,  jhill & jhawkes (1998).
    # "A normalized entropy measure of 3-D medical image alignment".
    # in Proc. Medical Imaging 1998, vol. 3338, San Diego, CA, pp. 132-143.
    if normalized:
        mi = ((np.sum(s1 * np.log(s1)) + np.sum(s2 * np.log(s2)))
                / np.sum(jh * np.log(jh))) - 1
    else:
        mi = ( np.sum(jh * np.log(jh)) - np.sum(s1 * np.log(s1))
               - np.sum(s2 * np.log(s2)))

    return mi

def calcEnt(Ti,m=40.,k=100, kernel='hard', temp=300):
    """
    Computes the momentum-, configurational- and total entropy in physical units.
    
    Parameters
    ----------
    Ti : array-like (n_samples, n_features)
         Trajectory of which entropy is computed.
    m  : float, optional
         Mass of particles in amu. Default: 40 (Argon).
    k  : int, optional
         k-Value of kNN-algorithm. Default: 100.
         
    Returns
    -------
    S  : 3-tuple
         S-momentum, S-conformation, S-total
    """
    shape = Ti.shape
    if len(shape) == 1:
        dof = 1.
    else:
        _,dof = shape
    
    S_mom = 1./2*kB*dof * (1 + np.log(kTJ*temp/300.*m*amu/(2*np.pi*hbar*hbar)))
    
    if kernel == 'hard':
        S_conf = kB * entropy(Ti, k=k) + dof*kB*np.log(nano) #last term corrects for units in nanometers
    elif kernel == 'gaussian':
        S_conf = kB * entropy_sk(Ti, k=k)[0] + dof*kB*np.log(nano) #last term corrects for units in nanometers
    return S_mom, S_conf, S_mom+S_conf

def gaussianEnt(var,m=40., temp=300):
    """
    Computes the total entropy of a harmonic oscillator in physical units.
    
    Parameters
    ----------
    var: float
         Variance of oscillator oscillation.
    m  : float, optional
         Mass of particles in amu. Default: 40 (Argon).

    Returns
    -------
    S  : 3-tuple
         S-momentum, S-conformation, S-total
    """
    SQH = kB * (0.5* np.log( uNmkTOverHbar2*temp/300. * m * var) + 1) #absolute entropy
    return SQH

def calcMI(Traj,i,j,k=100, tries=1, maxtries=1, fill_mode='double', verbouse=False, mode='mean'):
    """
    Computes the mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    i    : int
           index of first marginal
    j    : int
           index of second marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.
    tries: int, optional
           number of tries to either obtain a mean or a max value.
    mode : str, optional 
           Either 'max' or 'mean'; default: 'mean'. Thake mean or maximum of all tries.
    maxtries: int
           maximum number of tries after which calculation if continued, even if no non-negative value was found
    fill_mode: str, optional
           either 'double' or 'gaussian'; default: 'double'; 'gausian' is obsolete
           Convert the two 1D-entropies into 2D-entropies. Either by using a random permutation (double) or
           by multiplying with a gaussian.

    Returns
    -------
    MI : float
         mutual information
    """    

    if fill_mode not in ['double','gaussian']:
        sys.exit('Chosen fill mode does not exist.')
    if mode not in ['max','mean']:
        sys.exit('Chosen mode does not exist.')
                
    Ti = _reshape_(Traj[:,i])[0]
    Tj = _reshape_(Traj[:,j])[0]
    
    if fill_mode == 'gaussian':
        (_,eigenval,_),_ = mdab.anaeig(Traj,indist=False)
        eigenval = np.real(eigenval)
        std = gmean(np.sqrt(eigenval))
        gS = gaussianEnt(std*std)
    
    Sij = calcEnt(np.hstack([Ti,Tj]),k=k)[2]
    
    MI = -1.
    tryNo = 0
    
    while MI < 0 and tryNo < maxtries:
        
        #print tryNo
        sisj_tries = np.zeros(tries)
        for t in xrange(tries):
            if fill_mode == 'double':
                sisj = calcEnt(np.hstack([Ti,np.random.permutation(Tj)]),k=k)[2]
            elif fill_mode == 'gaussian':
                gaussian = np.random.normal(loc=0.0,scale=std,size=len(Traj))
                si = calcEnt(np.hstack([Ti,gaussian]),k=k)[2] - gS
                sj = calcEnt(np.hstack([Tj,gaussian]),k=k)[2] - gS
                sisj = si+sj
            
            sisj_tries[t] = sisj
                    
        if mode == 'mean':
            SiSj = np.mean(sisj_tries)
        elif mode == 'max':
            SiSj = np.max(sisj_tries)
        
        tryNo = tryNo + 1        
        MI = SiSj-Sij
    
    if MI <= 0 and verbouse:
        print "Found MI <= 0 (%f) for i=%4i, j=%4i after %i tries." %(MI,i,j, tryNo)
    
    return MI

def calcMI3(Traj,l,m,n,k=100, verbouse=False):
    """
    Computes the tripple mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    l    : int
           index of first marginal
    m    : int
           index of second marginal
    n    : int
           index of third marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.

    Returns
    -------
    MI : float
         mutual information
    """    
        
    Tl = _reshape_(Traj[:,l])[0]
    Tm = _reshape_(Traj[:,m])[0]
    Tn = _reshape_(Traj[:,n])[0]
        
    S_llmmnn = calcEnt(np.hstack([Tl,Tm,Tn]),k=k)[2]
    S_lmn    = calcEnt(np.hstack([np.random.permutation(Tl),np.random.permutation(Tm),np.random.permutation(Tn)]),k=k)[2]
    S_llmmn  = calcEnt(np.hstack([Tl,Tm,np.random.permutation(Tn)]),k=k)[2]
    S_llmnn  = calcEnt(np.hstack([Tl,np.random.permutation(Tm),Tn]),k=k)[2]
    S_lmmnn  = calcEnt(np.hstack([np.random.permutation(Tl),Tm,Tn]),k=k)[2]
    
    MI = 2.*S_lmn -S_llmmn -S_llmnn -S_lmmnn +S_llmmnn
        
    return MI

def calcEnt_qh(Traj,m=40.):
    """
    Computes the entropy using the Schlitter-approximation in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    m    : float, optional
           Mass of particles in amu. Default: 40 (Argon).
           
    Returns
    -------
    SS : float
         Schlitter entropy
    """        

    shape = Traj.shape
    
    if len(shape)>1:
        C = np.cov(Traj.T)
        eigenval, eigenvec = np.linalg.eig(C)
    else:
        eigenval = np.var(Traj)
    SS = 0.5 * kB * np.sum( np.log(1 + uNmkTOverHbar2 * np.exp(2) * m * eigenval))
    return SS

def calcMI_qh(Ti,Tj):
    """
    Computes the mutual information using the Schlitter-approximation in physical units.
    
    Parameters
    ----------
    Ti   : array-like (n_samples, n_features)
           First marginal trajectory.
    Tj   : array-like (n_samples, n_features)
           Second marginal trajectory.
           
    Returns
    -------
    MI   : float
           Mutual information
    """   
    
    si = calcEnt_qh(Ti)
    sj = calcEnt_qh(Tj)
    sij = calcEnt_qh(np.vstack([Ti,Tj]).T)
    return si+sj-sij

###############################################################################
# Tests
if __name__ == '__main__':

    def test_entropy():
        # Testing against correlated Gaussian variables
        # (analytical results are known)
        # Entropy of a 3-dimensional gaussian variable
        rng = np.random.RandomState(0)
        n = 50000
        d = 3
        P = np.array([[1, 0, 0], [0, 1, .5], [0, 0, 1]])
        C = np.dot(P, P.T)
        Y = rng.randn(d, n)
        X = np.dot(P, Y)
        H_th = entropy_gaussian(C)
        H_est = entropy(X.T, k=5)
        # Our estimated entropy should always be less that the actual one
        # (entropy estimation undershoots) but not too much
        np.testing.assert_array_less(H_est, H_th)
        np.testing.assert_array_less(.9*H_th, H_est)
    
    
    def test_mutual_information():
        # Mutual information between two correlated gaussian variables
        # Entropy of a 2-dimensional gaussian variable
        n = 50000
        rng = np.random.RandomState(0)
        #P = np.random.randn(2, 2)
        P = np.array([[1, 0], [0.5, 1]])
        C = np.dot(P, P.T)
        U = rng.randn(2, n)
        Z = np.dot(P, U).T
        X = Z[:, 0]
        X = X.reshape(len(X), 1)
        Y = Z[:, 1]
        Y = Y.reshape(len(Y), 1)
        # in bits
        MI_est = mutual_information((X, Y), k=5)
        MI_th = (entropy_gaussian(C[0, 0])
                 + entropy_gaussian(C[1, 1])
                 - entropy_gaussian(C)
                )
        # Our estimator should undershoot once again: it will undershoot more
        # for the 2D estimation that for the 1D estimation
        print  MI_est, MI_th
        np.testing.assert_array_less(MI_est, MI_th)
        np.testing.assert_array_less(MI_th, MI_est  + .3)
    
    
    def test_degenerate():
        # Test that our estimators are well-behaved with regards to
        # degenerate solutions
        rng = np.random.RandomState(0)
        x = rng.randn(50000)
        X = np.c_[x, x]
        assert np.isfinite(entropy(X))
        assert np.isfinite(mutual_information((x[:, np.newaxis],
                                               x[:,  np.newaxis])))
        assert 2.9 < mutual_information_2d(x, x) < 3.1
    
    
    def test_mutual_information_2d():
        # Mutual information between two correlated gaussian variables
        # Entropy of a 2-dimensional gaussian variable
        n = 50000
        rng = np.random.RandomState(0)
        #P = np.random.randn(2, 2)
        P = np.array([[1, 0], [.9, .1]])
        C = np.dot(P, P.T)
        U = rng.randn(2, n)
        Z = np.dot(P, U).T
        X = Z[:, 0]
        X = X.reshape(len(X), 1)
        Y = Z[:, 1]
        Y = Y.reshape(len(Y), 1)
        # in bits
        MI_est = mutual_information_2d(X.ravel(), Y.ravel())
        MI_th = (entropy_gaussian(C[0, 0])
                 + entropy_gaussian(C[1, 1])
                 - entropy_gaussian(C)
                )
        print  MI_est, MI_th
        # Our estimator should undershoot once again: it will undershoot more
        # for the 2D estimation that for the 1D estimation
        #np.testing.assert_array_less(MI_est, MI_th)
        #np.testing.assert_array_less(MI_th, MI_est  + .2)


    # Run our tests
    test_entropy()
    test_mutual_information()
    test_degenerate()
    test_mutual_information_2d() 
