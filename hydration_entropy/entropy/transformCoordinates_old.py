#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
pytools105
Copyright (C) 2018  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

This is to transform water trajectory coordinates from gromacs file formats
(.trr, .xtc) to a translational/rotational formulation. For the translational
part,  either the position of the oxygen atom (-trans oxy) or the position of
the center of mass (-trans com) can be used.
"""

import numpy as _np_
from ..mda_basics import loadTraj as _loadTraj_
from ..mda_basics import _print_commandline_stuff_
import sys as _sys_
import os as _os_

_command_ = 'transformCoordinates'

class transform:
    """
    This is to transform water trajectory coordinates from gromacs file formats
    (.trr, .xtc) to a translational/rotational formulation. For the translational
    part,  either the position of the oxygen atom (-trans oxy) or the position of
    the center of mass (-trans com) can be used.
    """
    
    def __init__(self, 
                 structure,                      # Input structure file [.gro, .tpr]
                 file,                           # Input trajectory file [.xtc, .trr]          
                 output        = '../traj.npz',  # Output file
                 masses        = (16.,1.,1.),    # Masses of atoms in u
                 test          = False,          # Test if quaternions are normalized
                 normalize     = True,           # Renormalize quaternions
                 begin         = 0,              # First frame to transform. Default = 0
                 end           = 0,              # Last frame to transform. Default = 0 (last) 
                 translation   = 'oxy',          # User either oxy-position or com as the translation coordinate. Default = oxy
                 box           = True,           # Include time-resolved box dimensions in output. If False, only average will be stored. 
                 tolerance     = 3,              # tolerance when checking normalization
                 ):
        
        self.structure  = structure
        self.file       = file
        self.output     = output
        self.masses     = masses
        self.test       = test
        self.normalize  = normalize
        self.begin      = begin
        self.end        = end
        self.translation= translation
        self.box        = box
        self.tolerance  = tolerance
        self.atomspermol= len(masses)
        
        if self.atomspermol not in [1, 3]:
            raise ValueError('"masses" needs to be a tuple of length 1 or 3.')
    
    # TODO: write this in C++
    def transform(self):
        """
        This is to transform water trajectory coordinates from gromacs file formats
        (.trr, .xtc) to a translational/rotational formulation. For the translational
        part,  either the position of the oxygen atom (-trans oxy) or the position of
        the center of mass (-trans com) can be used.
        """   
        #%% debugging functions
        #reference = np.array([[ 0.,  0.,  0.],
        #                      [-1.,  0.,  1.],
        #                      [ 1.,  0.,  1.]])
        #reference = reference - get_com(reference)
        #def artificialSample(Nframes,N):
        #    Q = np.zeros((Nframes,N,4))
        #    for t in xrange(Nframes):
        #        q, _ = createSample.createUniformSample(N)
        #        Q[t] = q
        #        
        #    Traj = np.zeros((Nframes,3*N,3))
        #    for t in xrange(Nframes):
        #        for i in xrange(N):
        #            q = Q[t,i,:]
        #            R = quaternion2matrix(q)
        #            x = np.dot(R.T,reference.T).T
        #            Traj[t,i*3:i*3+3] = x
        #        
        #    return Q, Traj
        
        #%%
        _sys_.stdout.write('Loading trajectory ...\n')
        _sys_.stdout.flush()
        Traj, Natm, Nframes, Box = _loadTraj_(self.structure, self.file)
        
        N = Natm/self.atomspermol
        
        Traj = Traj.reshape(Nframes,Natm,3)
        
        if self.end==0:
            Frames = range(Nframes)[self.begin:]
        else:
            Frames = range(Nframes)[self.begin:self.end]
        Nframes = len(Frames)
        
        if self.atomspermol > 1:
            Rot   = _np_.zeros((Nframes,N,4))
        else:
            Rot   = None
        Trans = _np_.zeros((Nframes,N,3))
        
        def quaternion2matrix(q):
            a, b, c, d = q
            
            R = _np_.array( [[a*a+b*b-c*c-d*d,     2*b*c-2*a*d,     2*b*d+2*a*c],
                             [    2*b*c+2*a*d, a*a-b*b+c*c-d*d,     2*c*d-2*a*b],
                             [    2*b*d-2*a*c,     2*c*d+2*a*b, a*a-b*b-c*c+d*d]])
            return R
        
        def matrix2quaternion(R):
        
            # Methode 1
            tr = _np_.trace(R)
            q = _np_.zeros(4)
            maxdiag = _np_.argmax(_np_.diag(R))
            
            if tr > 0:
                #print 0
                s = _np_.sqrt(tr+1.) * 2.
                q[0] = 0.25*s
                q[1] = (R[2,1]-R[1,2])/s
                q[2] = (R[0,2]-R[2,0])/s
                q[3] = (R[1,0]-R[0,1])/s
            elif 0 == maxdiag:
                #print 1
                s = _np_.sqrt(1.+R[0,0]-R[1,1]-R[2,2]) * 2.
                q[0] = (R[2,1]-R[1,2])/s
                q[1] = 0.25*s
                q[2] = (R[0,1]+R[1,0])/s
                q[3] = (R[0,2]+R[2,0])/s
            elif 1 == maxdiag:
                #print 2
                s = _np_.sqrt(1.+R[1,1]-R[0,0]-R[2,2]) * 2.
                q[0] = (R[0,2]-R[2,0])/s
                q[1] = (R[0,1]+R[1,0])/s
                q[2] = 0.25*s
                q[3] = (R[1,2]+R[2,1])/s
            else:
                #print 3
                s = _np_.sqrt(1.+R[2,2]-R[0,0]-R[1,1]) * 2.
                q[0] = (R[1,0]-R[0,1])/s
                q[1] = (R[0,2]+R[2,0])/s
                q[2] = (R[1,2]+R[2,1])/s   
                q[3] = 0.25*s
                        
            # Methode 2
            #slightly more efficient method, but has problems if argument of np.sign becomes 0    
        #    q2 = np.zeros(4)
        #    q2[0] = np.sqrt( max(0, 1+R[0,0]+R[1,1]+R[2,2]) ) /2.
        #    q2[1] = np.sqrt( max(0, 1+R[0,0]-R[1,1]-R[2,2]) ) /2.
        #    q2[2] = np.sqrt( max(0, 1-R[0,0]+R[1,1]-R[2,2]) ) /2.
        #    q2[3] = np.sqrt( max(0, 1-R[0,0]-R[1,1]+R[2,2]) ) /2.
        #    q2[1] = np.sign(R[2,1]-R[1,2]) * np.abs(q2[1])
        #    q2[2] = np.sign(R[0,2]-R[2,0]) * np.abs(q2[2])
        #    q2[3] = np.sign(R[1,0]-R[0,1]) * np.abs(q2[3])
                
            return q
        
        def get_com(molecule,masses=self.masses):
            # oxy, h1, h2 = molecule
            # return (masses[0]*oxy + masses[1]*h1 + masses[2]*h2)/float(sum(masses))
            return _np_.dot(_np_.array(masses), molecule)/float(sum(masses))
        
        
        def get_transCoord(molecule):
            com = get_com(molecule)
            oxy, h1, h2 = molecule - com
            
            #print np.linalg.norm(oxy-h1), np.linalg.norm(oxy-h2), np.linalg.norm(h1-h2)
                 
            #determine the current orientation of the molecule
            X = h2-h1
            X = X/_np_.linalg.norm(X)
            Z = -oxy/_np_.linalg.norm(oxy)
            Y = _np_.cross(Z,X)
            
            R = _np_.vstack([X,Y,Z])
            
            q = matrix2quaternion(R)
            
            if self.translation == 'com':
                return com,R,q
            elif self.translation == 'oxy':
                return oxy+com,R,q
            else:
                _sys_.exit('-trans must either be given "com" or "oxy".')
        
        #%% test quaternion conversion
        #maxDev = 0
        #for i in xrange(100000):
        #    q = createSample.createUniformSample(1)[0][0]
        #    qq = matrix2quaternion(quaternion2matrix(q))
        #    dev = metrics.geodesicNorm(q,qq)
        #    if dev > maxDev:
        #        maxDev = dev
        #        print maxDev
            
        #%% iterate
        for t in Frames:
            if t%100==0:
                _sys_.stdout.write('Transforming frame %i\n' %t)
                _sys_.stdout.flush()
            for i in xrange(N):
                mol = Traj[t][i*self.atomspermol:(i+1)*self.atomspermol]
                
                if self.atomspermol > 1:
                    com,_,q = get_transCoord(mol)
                    
                    if self.test or self.normalize:
                        norm = _np_.linalg.norm(q)                                
                        if _np_.round(norm,decimals=self.tolerance) != 1.0:
                            _sys_.exit('Quaternion not properly normalized! t=%i, i=%i: %f' %(t,i,norm))
                        if self.normalize:
                            q = q/norm    
                    Rot[t-self.begin,i] = q
                    Trans[t-self.begin,i] = com
                else:
                    Trans[t-self.begin,i] = mol
                
        _sys_.stdout.write('Saving files to disk ...\n')
                
        #%% save to file
        if self.box:
            _np_.savez(self.output, Trans = Trans, Rot = Rot, N = N, Nframes = Nframes,
                     trajFile = _os_.path.abspath(self.file), 
                     box_average = _np_.mean(Box,axis=0), Box = Box)
        else:
            _np_.savez(self.output, Trans = Trans, Rot = Rot, N = N, Nframes = Nframes,
                     trajFile = _os_.path.abspath(self.file), 
                     box_average=_np_.mean(Box,axis=0), Box=None)
        
        _sys_.stdout.write('... done.\n')
        
    def parse(self):
        """
        Parse the command-line.
        """
        
        command = ( '%s '
        '-s %s '
        '-f %s '
        '-o %s '
        '-m %f %f %f '
        '-t %r '
        '-norm %r '
        '-b %i '
        '-e %i '
        '-trans %s ' 
        '-box %s '
        '-tol %i'
        ) %(
                _command_,
                self.structure,
                self.file,
                self.output,
                self.masses[0], self.masses[1], self.masses[2],
                self.test,
                self.normalize,
                self.begin,
                self.end,
                self.translation,
                self.box,
                self.tolerance,
                )
        return command
        
#%% for commandline tool    
def __main__():
    
    _print_commandline_stuff_()
    
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--structure', '-s', type=str, help='Input structure file [.gro, .tpr]')
    parser.add_argument('--file', '-f', type=str, help='Input trajectory file [.xtc, .trr]')
    parser.add_argument('--output', '-o', type=str, help='Output file.', default='../traj.npz')
    parser.add_argument('--masses', '-m', type=tuple, help='Masses of atoms in u.', default=(16.,1.,1.))
    parser.add_argument('--test', '-t', type=bool, help='Test if quaternions are normalized.', default=False)
    parser.add_argument('--normalize', '-norm', type=bool, help='Renormalize quaternions.', default=True)
    parser.add_argument('--begin', '-b', type=int, help='First frame to transform. Default = 0.', default=0)
    parser.add_argument('--end', '-e', type=int, help='Last frame to transform. Default = 0 (last)', default=0)
    parser.add_argument('--translation', '-trans', type=str, help='User either oxy-position or com as the translation coordinate. Default = oxy', default='oxy')
    parser.add_argument('--box', '-box', type=str, default=True, help='Include time-resolved box dimensions in output. If False, only average will be stored.')
    parser.add_argument('--tolerance', '-tol', type=int, help='Tolerance when checking normalization. Throws error, it quaternion is not normalized within given number of decimals.', default=3)
    
    args = parser.parse_args()
        
    t = transform(  structure    = args.structure,
                    file          = args.file,
                    output        = args.output,
                    masses        = args.masses,
                    test          = args.test,
                    normalize     = args.normalize,
                    begin         = args.begin,
                    end           = args.end,
                    translation   = args.translation,
                    box           = args.box,
                    tolerance     = args.tolerance,
                    )
    t.transform()
