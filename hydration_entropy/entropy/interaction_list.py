 #!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2020  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

This module does a mutual information expansion in order to obtain
entropy values from a permutationally reduced trajectory.

"""

_command_ = 'interaction_list'

import sys as _sys_
import os as _os_
import numpy as _np_
from ..mda_basics import getNeighbors as _getNeighbors_
from ..mda_basics import _print_commandline_stuff_
    
class interaction_list:
    """
    Generates a list of interactions (pairs/triples) to be used for mutual information calculation.
    Inputs:
        input_traj:         name of input trajectory file
        order:              order of expansion
        output_file = None: name of output file - will be automatically named after order
        wd = None:          working directory- if None, current dir. will be used.
        cutoff = 0:         Cutoff for neighbors to be considered. 0 = infinity. Default: 0.
    """
    
    def __init__(self,
                 input_traj,         # name of input trajectory file
                 order,              # order of expansion
                 output_file = None, # name of output file - will be automatically named after order
                 wd = None,          # working directory- if None, current dir. will be used
                 cutoff = 0.0,        # cutoff for neighbors to be considered. 0 = infinity. Default: 0.
                 ):
        
        self.input_traj = input_traj
        if order in [2,3,12,21]:
            self.order = order
        else:
            raise ValueError('Order needs to be either 2 or 3 to generate a pair/triples-list and 12 or 21 to generate triple-lists for trans-rot-rot and trans-trans-rot correlations.')

        if output_file == None:
            if order == 2:
                self.output_file = 'pair_list.npz'
            elif order == 3:
                self.output_file = 'triple_list.npz'
            elif order == 21:
                self.output_file = 'triple_21_list.npz'
            elif order == 12:
                self.output_file = 'triple_12_list.npz'
        else:
            self.output_file = output_file
        
        if wd == None:
            wd = _os_.getcwd()
        else:
            if not _os_.path.isabs(wd):
                wd = _os_.getcwd()+'/'+wd
        self.wd = wd
                
        self.cutoff = cutoff
                             
    def search(self):
        """
        Search for inateractions.
        """

        _sys_.stdout.write( 'Loading trajectory...\n' )
        _sys_.stdout.flush()
        filename = self.wd + '/' + self.input_traj
        D = _np_.load(filename)
        N = int(D['N'])
        Nframes = int(D['Nframes'])
        TrajT = D['Trans'].reshape((Nframes,3*N))
        boxSize = D['box_average']
        
        _sys_.stdout.write( 'Looking for interactions...\n' )
        _sys_.stdout.flush()
                
        interact = _getNeighbors_(TrajT, 
                               cut = self.cutoff,
                               partners = self.order,
                               L = boxSize,
                               dofIndex = False,
                               getSelf = False,
                               symmetrize = False)
        
        _sys_.stdout.write( 'Found %i interactions.\n' %len(interact))
        _sys_.stdout.flush()
        
        _np_.savez(self.wd + '/' + self.output_file,
                 filename=filename, cutoff=self.cutoff, interactions=interact)
    
        
    def parse(self):
        """
        Parse the a command line to run the job.
        """
        
        command = ( '%s '
                '-f %s '
                '-or %i '
                '-o %s '
                '-wd %s '
                '-cut %f '
                ) %(
                        _command_,
                        self.input_traj,
                        self.order,
                        self.output_file,
                        self.wd,
                        self.cutoff,
                        )
        return command
        
#%% for commandline tool    
def __main__():
    
    _print_commandline_stuff_()
    
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--file',   '-f',   type=str, 
                        help='Name of input trajectory file.')
    parser.add_argument('--order',  '-or',  type=int, 
                        help='Order of expansion.')
    parser.add_argument('--output', '-o',   type=str, 
                        help='Name of output file. Will be named after order if not given.',
                        default=None)
    parser.add_argument('--workdir','-wd',  type=str, 
                        help='Working directory. If not given, current dir. will be used.',
                        default=None)  
    parser.add_argument('--cutoff','-cut',  type=float, 
                        help='Cutoff for neighbors to be considered. 0 = infinity. Default: 0.',
                        default=0.0) 
    args = parser.parse_args()
                    
    M = interaction_list(
            input_traj  = args.file,
            order       = args.order,
            output_file = args.output,
            wd          = args.workdir,
            cutoff      = args.cutoff,
            )
    M.search()
