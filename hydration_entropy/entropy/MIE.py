 #!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2020  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

This module does a mutual information expansion in order to obtain
entropy values from a permutationally reduced trajectory.

"""

_command_ = 'runMIE'

import sys as _sys_
import os as _os_
import gc as _gc_
import numpy as _np_
from multiprocessing import Pool as _Pool_
from ..mda_basics import loadTraj as _loadTraj_
from ..mda_basics import particleNdx2dof as _particleNdx2dof_
from ..mda_basics import  particleNdx2dof_rot as _particleNdx2dof_rot_
from ..mda_basics import getNeighbors as _getNeighbors_
from ..mda_basics import _print_commandline_stuff_
from .kNN_trans import calcEnt as _calcEnt_trans_
from .kNN_trans import calcMI as _calcMI_trans_
from .kNN_trans import calcMI3 as _calcMI3_trans_
from .kNN_rot import calcEnt as _calcEnt_rot_
from .kNN_rot import calcMI as _calcMI_rot_
from .kNN_rot import calcMI3 as _calcMI3_rot_
from .kNN_mix import calcMI as _calcMI_mix_
from .kNN_mix import calcMI21 as _calcMI21_mix_
from .kNN_mix import calcMI12 as _calcMI12_mix_

def save2file(order, Data, filename, k, Nframes, cutoff=0, header=''):
    f = open(filename,'w')
    f.write(header+'\n')
    f.write('Nframes = %i    k = %f    cutoff = %f\n' %(Nframes,k,cutoff))
    
    for d,i in zip(Data,range(len(Data))):
        if order == 1:
            f.write('%4i    %10.6f\n' %(i,d))
        elif order == 2:
            ndx = list(d[0])
            f.write('%4i    %4i    %+10.6f\n' %(ndx[0],ndx[1],d[1]))
        elif order in [3, 12, 21]:
            ndx = list(d[0])
            f.write('%4i    %4i    %4i    %+10.6f\n' %(ndx[0],ndx[1],ndx[2],d[1]))
            
    f.close()
    
def load2disk(order, filename):
    f = open(filename, 'r')
    
    # read header
    line = f.readline()
    if line.strip() != '':
        Sum = float(line.strip().split('=')[-1])
    else:
        Sum = None
    line = f.readline()
    if line.strip() != '':
        Nframes, k, cutoff = [ float(x.split('=')[-1]) for x in line.split('    ')]
        Nframes, k = int(Nframes), int(k)
    else:
        Nframes, k, cutoff = None, None, None
    Header = {'Sum': Sum, 'Nframes': Nframes, 'k': k, 'cutoff': cutoff}
    
    # read rest
    D = []        
    for line in f:
        columns = line.split()
        if order == 1:
            D.append(float(columns[1]))
        elif order == 2:
            D.append(([int(columns[0]), int(columns[1])], float(columns[2]) ))
        elif order in [3, 12, 21]:
            D.append(([int(columns[0]), int(columns[1]), int(columns[2]) ], float(columns[3]) ))
    f.close()
    return D, Header

def stitch(order, filenames, output_name, wd = None):
    """
    Stitch files of different parts of a calculation together.
    Input:
        order
        filenames (list of str)
        output_name
        wd: working directory; if not given, use current one
    """
    if wd == None:
        wd = _os_.getcwd()
    else:
        if not _os_.path.isabs(wd):
            wd = _os_.getcwd()+'/'+wd
    
    Nframes_ = []
    k_ = []
    cutoff_ = []
    D = []
    
    for filename in filenames:
        d, header = load2disk(order, wd+'/'+filename)
        Nframes_.append(header['Nframes'])
        k_.append(header['k'])
        cutoff_.append(header['cutoff'])
        D = D + d
    
    # determine header entries    
    def all_identical(lst):
        return not lst or lst.count(lst[0]) == len(lst)
    
    if all_identical(Nframes_):
        Nframes = Nframes_[0]
    else:
        Nframes = -1
        print 'Warning: Nframes are not identical in file headers.'
    if all_identical(k_):
        k = k_[0]
    else:
        k = -1
        print 'Warning: k are not identical in file headers.'
    if all_identical(cutoff_):
        cutoff = cutoff_[0]
    else:
        cutoff = -1
        print 'Warning: cutoff are not identical in file headers.'
        
    # calculate sum
    if order == 1:
        Sum = sum(D)
    else:
        Sum = sum([x[-1] for x in D])
        
    # write 2 file
    save2file(order, D, wd+'/'+output_name, k, Nframes, cutoff=cutoff,
              header='Sum = %f' %Sum)
    
def _outerLoop2_trans_(pair):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1, p2 = map( _particleNdx2dof_, pair )
    mi = _calcMI_trans_(Traj,
                        p1, p2,
                        k = k )
    _sys_.stdout.write( '%4i    %4i    %+10.6f\n' %(list(pair)[0],list(pair)[1],mi) )
    _sys_.stdout.flush()
    return (pair,mi)

def _outerLoop2_rot_(pair):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1, p2 = map( _particleNdx2dof_rot_, pair )
    mi = _calcMI_rot_(Traj,
                      p1, p2,
                      k = k )
    _sys_.stdout.write( '%4i    %4i    %+10.6f\n' %(list(pair)[0],list(pair)[1],mi) )
    _sys_.stdout.flush()
    return (pair,mi)

_translational_scaling_ = 10.
def _outerLoop2_mix_(pair):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.   
    p1 = _particleNdx2dof_(pair[0])
    p2 = _particleNdx2dof_rot_(pair[1])
    mi = _calcMI_mix_(TrajTrans, Traj,
                      p1, p2,
                      k = k )
    _sys_.stdout.write( '%4i    %4i    %+10.6f\n' %(list(pair)[0],list(pair)[1],mi) )
    _sys_.stdout.flush()
    return (pair,mi)

def _outerLoop21_mix_(triple):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1 = _particleNdx2dof_(triple[0])
    p2 = _particleNdx2dof_(triple[1])
    p3 = _particleNdx2dof_rot_(triple[2])
    mi = _calcMI21_mix_(TrajTrans, Traj, p1, p2, p3, k = k)
    _sys_.stdout.write( '%4i    %4i    %4i    %+10.6f\n' %(list(triple)[0],list(triple)[1],list(triple)[2], mi) )
    _sys_.stdout.flush()
    return (triple,mi)

def _outerLoop12_mix_(triple):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1 = _particleNdx2dof_(triple[0])
    p2 = _particleNdx2dof_rot_(triple[1])
    p3 = _particleNdx2dof_rot_(triple[2])
    mi = _calcMI12_mix_(TrajTrans, Traj, p1, p2, p3, k = k)
    _sys_.stdout.write( '%4i    %4i    %4i    %+10.6f\n' %(list(triple)[0],list(triple)[1],list(triple)[2], mi) )
    _sys_.stdout.flush()
    return (triple,mi)

def _outerLoop3_trans_(triple):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1, p2, p3 = map( _particleNdx2dof_, triple )
    mi = _calcMI3_trans_(Traj, p1, p2, p3, k = k)
    _sys_.stdout.write( '%4i    %4i    %4i    %+10.6f\n' %(list(triple)[0],list(triple)[1],list(triple)[2], mi) )
    _sys_.stdout.flush()
    return (triple,mi)

def _outerLoop3_rot_(triple):
    # TODO: This is a diry hack and relies on global variable Traj and k, still
    # to be defined later in the code. Get rid of it when possible.
    p1, p2, p3 = map( _particleNdx2dof_rot_, triple )
    mi = _calcMI3_rot_(Traj, p1, p2, p3, k = k)
    _sys_.stdout.write( '%4i    %4i    %4i    %+10.6f\n' %(list(triple)[0],list(triple)[1],list(triple)[2], mi) )
    _sys_.stdout.flush()
    return (triple,mi)

def _seed_internal_(x):
    _np_.random.seed(x)
    return 0
def _seed_(p):
    """ 
    Seed the numpy random number generators for a pool of workers. 
    """
    p.map(_seed_internal_, _np_.random.randint(4294967295, size=len(p._pool)))
    
class mie:
    """
    Class to perform a mutual-information expansion calculation.
    Inputs:
        input_traj:         name of input trajectory file
        space:              'trans' or 'rot'
        order:              order of expansion
        k = 1:              nearest neighbor to consider
        parts = '1/1':      'n/m'; perform nth out of m parts
        threads = 1:        number of threads - if 0, use number of available cores
        output_file = None: name of output file - will be automatically named after order
        wd = None:          working directory- if None, current dir. will be used.
        m = 18.0            Mass of water molecule in amu.
                            Only relevant for 'trans'. Default: 18.0.
        I = 'O':            array of length 3 or str, optional. Only relevant for 'rot'.
                            Moment of inertia of molecule in SI units. 
                            As string, either "COM" or "O" depending 
                            on the pivot point used for water analysis. Default: "O".
        cutoff = 0:         Cutoff for neighbors to be considered. 0 = infinity. Default: 0.
        temp = 300:         Temperature of the simulation.
        intlist = None:     npz-file containg a list of pairs/triples for the MIE. 
                            If given, cutoff is ignored and interactions are taken from the list instead.
    """
    
    def __init__(self,
                 input_traj,         # name of input trajectory file
                 space,              # 'trans' or 'rot'
                 order,              # order of expansion
                 k = 1,              # nearest neighbor to consider
                 parts = '1/1',      # 'n/m'; perform nth out of m parts
                 threads = 1,        # number of threads
                 output_file = None, # name of output file - will be automatically named after order
                 wd = None,          # working directory- if None, current dir. will be used
                 m = 18.0,           # mass of water molecule
                 I = 'O',            # array of length 3 or str, optional.
                                     # Moment of inertia of molecule in SI units. 
                                     # As string, either "COM" or "O" depending 
                                     # on the pivot point used for water analysis. Default: "O".
                cutoff = 0.0,        # cutoff for neighbors to be considered. 0 = infinity. Default: 0.
                temp = 300,          # temperature of the simulation
                metric = None,       # metric to be used
                intlist = None       # npz-file containg a list of pairs/triples for the MIE
                 ):
        
        self.input_traj = input_traj
        
        if space not in ['trans', 'rot', 'mix']:
            raise ValueError("'space' must be either 'trans', 'rot' or 'mix'.")
        self.space = space
        if space == 'trans' and (order < 1 or order > 3):
            raise ValueError("'order' must be between 1 and 3 for space 'trans'.")
        if space == 'rot' and (order < 1 or order > 3):
            raise ValueError("'order' must be between 1 and 3 for space 'rot'.")
        if space == 'mix' and order not in [2, 21, 12]:
            raise ValueError("'order' must be 2, 12, or 21 for space 'mix'.")
        
        self.order = order
        
        self.part, self.n_parts = map(int, parts.split('/'))
        
        self.threads = threads
        
        if self.n_parts > 1:
            out_sufix = '.%03i' %self.part
        else:
            out_sufix = ''
        if output_file == None:
            if order == 1:
                self.output_file = '1stOrder.dat%s' %out_sufix
            elif order == 2:
                self.output_file = '2ndOrder.dat%s' %out_sufix
            elif order == 3:
                self.output_file = '3rdOrder.dat%s' %out_sufix
            elif order == 21:
                self.output_file = '2-1-Order.dat%s' %out_sufix
            elif order == 12:
                self.output_file = '1-2-Order.dat%s' %out_sufix
        else:
            self.output_file = output_file + out_sufix
        
        if wd == None:
            wd = _os_.getcwd()
        else:
            if not _os_.path.isabs(wd):
                wd = _os_.getcwd()+'/'+wd
        self.wd = wd
        
        if self.n_parts > 1:
            self.try_stitch = True
        else:
            self.try_stitch = False
        
        self.k = k
        self.m = m
        self.I = I
        self.cutoff = cutoff
        self.temp = temp
        if str(metric).lower() == 'none':
            self.metric = None
        else:
            self.metric = metric
            
        if str(intlist).lower() == 'none':
            self.intlist = None
        else:
            self.intlist = intlist
        
    def set_parts(self, parts):
        """
        Sets the parts in which the calculation is performed. 
        E.g. mie.set_parts('3/10') splits the calculation in 10 parts and 
        performs part 3 now.
        """
        self.part, self.n_parts = map(int, parts.split('/'))
            
    def __try_stitch__(self, remove_old=False):
        """
        Try stitching the output files of the different parts together to form
        a single file. This assumes that the individual files are named
        $(filename).n for n being the part number.
        """
        if self.n_parts == 1:
            return
        
        name = self.output_file.rsplit('.',1)[0]
        names = [name+'.%03i' %i for i in range(1,self.n_parts+1)]
        
        for n in names:
            # check if file exist
            if not _os_.path.isfile(self.wd+'/'+n):
                return 
            
        stitch(self.order, names, name, wd = self.wd)
        
    def __run_1__(self, Traj, N, Nframes):
        
        _sys_.stdout.write( 'Calculating 1. order entropy...\n' )
        _sys_.stdout.flush()
        
        S = _np_.zeros(N)
        
        if self.space == 'trans':
            calcEnt = _calcEnt_trans_
            args = {'k': self.k, 'm': self.m, 'temp': self.temp}
            p2d = _particleNdx2dof_
        else:
            calcEnt = _calcEnt_rot_
            args = {'k': self.k, 'I': self.I, 'temp': self.temp, 'metric': self.metric}
            p2d = _particleNdx2dof_rot_
        
        for i in xrange(N):
            S[i] = calcEnt( Traj[:, p2d(i)], **args )[2]
            _sys_.stdout.write(str(S[i])+'\n')
            _sys_.stdout.flush()
            
            
        _sys_.stdout.write( 'Total 1. order entropy is %f J/mol/K.\n' %_np_.sum(S) )
        save2file(order = 1, 
                  Data = S, 
                  filename = self.wd + '/' + self.output_file, 
                  Nframes = Nframes, 
                  k = self.k, 
                  cutoff = 0,
                  header='Sum = %f' %_np_.sum(S))
        
    def __run_2__(self, Trajj, TrajT, N, Nframes, boxSize):
        _sys_.stdout.write( 'Looking for pairs...\n' )
        _sys_.stdout.flush()
                
        if self.intlist is None:
            Pairs = _getNeighbors_(TrajT, 
                                   cut = self.cutoff,
                                   partners = 2,
                                   L = boxSize,
                                   dofIndex = False,
                                   getSelf = False,
                                   symmetrize = False)
        else:
            _sys_.stdout.write( 'Found list of interactions.\n' )
            _sys_.stdout.flush()            
            Pairs = _np_.load(self.wd + '/' + self.intlist)['interactions']
        
        if self.space == 'mix':
            # TODO: This needs global variable to be passed to the parallel map.
            # Get rit of this.
            global TrajTrans
            TrajTrans = TrajT * _translational_scaling_
            
            # add self-correlations and inverted pairs
            Selfpairs = _np_.array([[i,i] for i in xrange(N)])
            Invpairs = _np_.array([[p[1], p[0]] for p in Pairs ])
            if len(Pairs) == 0:
                Pairs = Selfpairs
            else:
                Pairs = _np_.concatenate([Selfpairs, Pairs, Invpairs])
        
        # delete TrajT, as it is not needed any more
        del TrajT
        _gc_.collect()
        
        Pairs = _np_.array_split(Pairs, self.n_parts)[self.part - 1]
        _sys_.stdout.write( 'Found %i pairs of dofs.\n' %len(Pairs) )
        _sys_.stdout.write( 'Starting calculation of pairwise mutual information of %i dofs.\n' %len(Pairs) )
        
        # TODO: This needs global variable to be passed to the parallel map.
        # Get rit of this.
        global k
        global Traj
        global metric
        k = self.k
        Traj = Trajj
        metric = self.metric
                
        if self.threads > 1:
            p = _Pool_(self.threads)
            _seed_(p)
            _sys_.stdout.write( 'Started pool (%i) for mi calculation.\n' %self.threads )
            _sys_.stdout.flush()
            try:
                if self.space == 'trans':
                    I = p.map( _outerLoop2_trans_, Pairs )
                elif self.space == 'rot':
                    I = p.map( _outerLoop2_rot_, Pairs )
                elif self.space == 'mix':
                    I = p.map( _outerLoop2_mix_, Pairs )
            finally:
                p.close()
        else:
            _sys_.stdout.write( 'Started sequencial mi calculation.\n' )
            _sys_.stdout.flush()
            if self.space == 'trans':
                I = map( _outerLoop2_trans_, Pairs )
            elif self.space == 'rot':
                I = map( _outerLoop2_rot_, Pairs )
            elif self.space == 'mix':
                I = map( _outerLoop2_mix_, Pairs )
       
        MI_exact = sum([mi[1] for mi in I])
        
        _sys_.stdout.write( '2. order MI, taking %i dofs into account, is %f J/mol/K.\n' %(len(Pairs),MI_exact) )
        save2file(order = 2,
                  Data = I,
                  filename = self.wd + '/' + self.output_file, 
                  Nframes = Nframes,
                  k = self.k,
                  cutoff = self.cutoff, 
                  header='Sum = %f' %MI_exact)
        
    def __run_21__(self, Trajj, TrajT, N, Nframes, boxSize):
        _sys_.stdout.write( 'Looking for tripples...\n' )            
        Triples = _np_.load(self.wd + '/' + self.intlist)['interactions']
        _sys_.stdout.write( 'Found list of interactions.\n' )
        _sys_.stdout.flush()
        
        global TrajTrans
        TrajTrans = TrajT * _translational_scaling_
            
        # delete TrajT, as it is not needed any more
        del TrajT
        _gc_.collect()
        
        Triples = _np_.array_split(Triples, self.n_parts)[self.part - 1]
        _sys_.stdout.write( 'Found %i tripples of dofs.\n' %len(Triples) )
        # calculate tripplewise mutual information   
        _sys_.stdout.write( 'Starting calculation of tripple mutual information of %i triples. \n' %len(Triples) )
        
        # TODO: This needs global variable to be passed to the parallel map.
        # Get rit of this.      
        global k
        global Traj
        global metric
        k = self.k
        Traj = Trajj
        metric = self.metric
        
        if self.threads > 1:
            p = _Pool_(self.threads)
            _seed_(p)
            _sys_.stdout.write( 'Started pool (%i) for mi calculation.\n' %self.threads )
            _sys_.stdout.flush()
            try:
                if self.space == 'mix':
                    I2 = p.map( _outerLoop21_mix_, Triples )
                else:
                    _sys_.exit('Space needs to be "mixed" for order 21.')
            finally:
                p.close()
        else:
            _sys_.stdout.write( 'Started sequencial mi calculation.\n' )
            _sys_.stdout.flush()
            if self.space == 'mix':
                I2 = map( _outerLoop21_mix_, Triples )
            else:
                _sys_.exit('Space needs to be "mixed" for order 21.')
        
        MI2_exact = sum([mi[1] for mi in I2])
        
        _sys_.stdout.write( '3. order MI, taking %i tripples into account, is %f J/mol/K.\n' %(len(Triples), MI2_exact) )
        save2file(order = 3,
                  Data = I2,
                  filename = self.wd + '/' + self.output_file, 
                  Nframes = Nframes,
                  k = self.k,
                  cutoff = self.cutoff, 
                  header='Sum = %f' %MI2_exact)
        
    def __run_12__(self, Trajj, TrajT, N, Nframes, boxSize):
        _sys_.stdout.write( 'Looking for tripples...\n' )            
        Triples = _np_.load(self.wd + '/' + self.intlist)['interactions']
        _sys_.stdout.write( 'Found list of interactions.\n' )
        _sys_.stdout.flush()
        
        global TrajTrans
        TrajTrans = TrajT * _translational_scaling_
            
        # delete TrajT, as it is not needed any more
        del TrajT
        _gc_.collect()
        
        Triples = _np_.array_split(Triples, self.n_parts)[self.part - 1]
        _sys_.stdout.write( 'Found %i tripples of dofs.\n' %len(Triples) )
        # calculate tripplewise mutual information   
        _sys_.stdout.write( 'Starting calculation of tripple mutual information of %i triples. \n' %len(Triples) )
        
        # TODO: This needs global variable to be passed to the parallel map.
        # Get rit of this.      
        global k
        global Traj
        global metric
        k = self.k
        Traj = Trajj
        metric = self.metric
        
        if self.threads > 1:
            p = _Pool_(self.threads)
            _seed_(p)
            _sys_.stdout.write( 'Started pool (%i) for mi calculation.\n' %self.threads )
            _sys_.stdout.flush()
            try:
                if self.space == 'mix':
                    I2 = p.map( _outerLoop12_mix_, Triples )
                else:
                    _sys_.exit('Space needs to be "mixed" for order 21.')
            finally:
                p.close()
        else:
            _sys_.stdout.write( 'Started sequencial mi calculation.\n' )
            _sys_.stdout.flush()
            if self.space == 'mix':
                I2 = map( _outerLoop12_mix_, Triples )
            else:
                _sys_.exit('Space needs to be "mixed" for order 12.')
        
        MI2_exact = sum([mi[1] for mi in I2])
        
        _sys_.stdout.write( '3. order MI, taking %i tripples into account, is %f J/mol/K.\n' %(len(Triples), MI2_exact) )
        save2file(order = 3,
                  Data = I2,
                  filename = self.wd + '/' + self.output_file, 
                  Nframes = Nframes,
                  k = self.k,
                  cutoff = self.cutoff, 
                  header='Sum = %f' %MI2_exact)
        
    def __run_3__(self, Trajj, TrajT, N, Nframes, boxSize):
        _sys_.stdout.write( 'Looking for tripples...\n' )
        _sys_.stdout.flush()
        
        if self.intlist is None:
            Triples = _getNeighbors_(TrajT,
                                     cut = self.cutoff,
                                     partners = 3,
                                     L = boxSize,
                                     dofIndex = False,
                                     getSelf = False) 
        else:
            _sys_.stdout.write( 'Found list of interactions.\n' )
            _sys_.stdout.flush()            
            Triples = _np_.load(self.wd + '/' + self.intlist)['interactions']
            
        # delete TrajT, as it is not needed any more
        del TrajT
        _gc_.collect()
        
        Triples = _np_.array_split(Triples, self.n_parts)[self.part - 1]
        _sys_.stdout.write( 'Found %i tripples of dofs.\n' %len(Triples) )
        # calculate tripplewise mutual information   
        _sys_.stdout.write( 'Starting calculation of tripple mutual information of %i triples. \n' %len(Triples) )
        
        # TODO: This needs global variable to be passed to the parallel map.
        # Get rit of this.      
        global k
        global Traj
        global metric
        k = self.k
        Traj = Trajj
        metric = self.metric
        
        if self.threads > 1:
            p = _Pool_(self.threads)
            _seed_(p)
            _sys_.stdout.write( 'Started pool (%i) for mi calculation.\n' %self.threads )
            _sys_.stdout.flush()
            try:
                if self.space == 'trans':
                    I2 = p.map( _outerLoop3_trans_, Triples )
                else:
                    I2 = p.map( _outerLoop3_rot_, Triples )
            finally:
                p.close()
        else:
            _sys_.stdout.write( 'Started sequencial mi calculation.\n' )
            _sys_.stdout.flush()
            if self.space == 'trans':
                I2 = map( _outerLoop3_trans_, Triples )
            else:
                I2 = map( _outerLoop3_rot_, Triples )
        
        MI2_exact = sum([mi[1] for mi in I2])
        
        _sys_.stdout.write( '3. order MI, taking %i tripples into account, is %f J/mol/K.\n' %(len(Triples), MI2_exact) )
        save2file(order = 3,
                  Data = I2,
                  filename = self.wd + '/' + self.output_file, 
                  Nframes = Nframes,
                  k = self.k,
                  cutoff = self.cutoff, 
                  header='Sum = %f' %MI2_exact)
     
    def run(self):
        """
        Run the job.
        """
        
        # set number of threads to use
        if self.threads == 0:
            from multiprocessing import cpu_count
            self.threads = cpu_count()
        
        _sys_.stdout.write( 'Loading trajectory...\n' )
        _sys_.stdout.flush()
        filename = self.wd + '/' + self.input_traj
        if self.space == 'trans':
            Traj, _, N, Nframes, _, boxSize = _loadTraj_(filename,
                                                         filename)
            TrajT = Traj
        else:
            TrajT, Traj, N, Nframes, _, boxSize = _loadTraj_(filename,
                                                             filename)           
        
        if self.order == 1:
            self.__run_1__(Traj, N, Nframes)
        elif self.order == 2:
            self.__run_2__(Traj, TrajT, N, Nframes, boxSize)
        elif self.order == 3:
            self.__run_3__(Traj, TrajT, N, Nframes, boxSize)
        elif self.order == 21:
            self.__run_21__(Traj, TrajT, N, Nframes, boxSize)
        elif self.order == 12:
            self.__run_12__(Traj, TrajT, N, Nframes, boxSize)
                   
        self.__try_stitch__(remove_old=True)
        
    def parse(self):
        """
        Parse the a command line to run the job.
        """
        
        if len(self.I) > 1:
            I = str(self.I)[1:-1]
        else:
            I = self.I
            
        if self.n_parts > 1:
            outname = self.output_file.rsplit('.',1)[0]
        else:
            outname = self.output_file
        
        command = ( '%s '
                '-f %s '
                '-sp %s '
                '-or %i '
                '-pa %i/%i '
                '-nt %i '
                '-o %s '
                '-wd %s '
                '-m %f '
                '-I %s '
                '-cut %f '
                '-k %i '
                '-temp %f '
                '-metric %s '
                '-intlist %s '
                ) %(
                        _command_,
                        self.input_traj,
                        self.space,
                        self.order,
                        self.part, self.n_parts,
                        self.threads,
                        outname,
                        self.wd,
                        self.m,
                        I,
                        self.cutoff,
                        self.k,
                        self.temp,
                        self.metric,
                        self.intlist,
                        )
        return command
        
#%% for commandline tool    
def __main__():
    
    _print_commandline_stuff_()
    
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--file',   '-f',   type=str, 
                        help='Name of input trajectory file.')
    parser.add_argument('--space',  '-sp',  type=str, 
                        help='Space in which to perform calculation (trans or rot).')
    parser.add_argument('--order',  '-or',  type=int, 
                        help='Order of expansion.')
    parser.add_argument('--parts',  '-pa',  type=str, 
                        help="'n/m'; Perform nth out of m parts. Default: 1/1.",
                        default='1/1')    
    parser.add_argument('--threads','-nt',  type=int, 
                        help='Number of threads. Default: 1. Takes all available cores if 0.',
                        default=1)
    parser.add_argument('--output', '-o',   type=str, 
                        help='Name of output file. Will be named after order if not given.',
                        default=None)
    parser.add_argument('--workdir','-wd',  type=str, 
                        help='Working directory. If not given, current dir. will be used.',
                        default=None)  
    parser.add_argument('--mass','-m',  type=float, 
                        help='Mass of water molecule in amu. Only relevant for "trans". Default: 18.0.',
                        default=18.0)  
    parser.add_argument('--inertia','-I', nargs='+',
                        help =  ('Array of length 3 or str, optional. Only relevant for "rot". ' 
                                 'Moment of inertia of molecule in SI units. ' 
                                 'As string, either "COM" or "O" depending ' 
                                 'on the pivot point used for water analysis. Default: "O".'),
                        default='O') 
    parser.add_argument('--cutoff','-cut',  type=float, 
                        help='Cutoff for neighbors to be considered. 0 = infinity. Default: 0.',
                        default=0.0) 
    parser.add_argument('--kNN','-k',  type=int, 
                        help='k-nearest neighbor to use for MIE.',
                        default=1) 
    parser.add_argument('--temperature','-temp',  type=float, 
                        help='Temperature of the simulation.',
                        default=300)
    parser.add_argument('--metric','-metric',  type=str, 
                        help='Metric to be used for distance calculation. If not given, standard values are chosen.',
                        default=None)
    parser.add_argument('--interaction_list','-intlist',  type=str, 
                        help='npz-file containg a list of pairs/triples for the MIE. If given, cutoff is ignored and interactions are taken from the list instead.',
                        default=None)
    args = parser.parse_args()
    
    if args.inertia not in ['O', 'COM']:
        args.inertia = _np_.array(args.inertia)
                
    M = mie(
            input_traj  = args.file,
            space       = args.space,
            order       = args.order,
            parts       = args.parts,
            threads     = args.threads,
            output_file = args.output,
            wd          = args.workdir,
            m           = args.mass,
            I           = args.inertia,
            cutoff      = args.cutoff,
            k           = args.kNN,
            temp        = args.temperature,
            metric      = args.metric,
            intlist     = args.interaction_list
            )
    M.run()
