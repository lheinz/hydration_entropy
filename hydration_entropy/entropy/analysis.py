import numpy as _np_
import os as _os_
from ..mda_basics import writeBfactorPDB as _writeBfactorPDB_
from ..mda_basics import loadGRO as _loadGRO_
from ..mda_basics import readNdx as _readNdx_
from ..mda_basics import writeNdx as _writeNdx_
from ..mda_basics import _print_commandline_stuff_
from ..mda_basics import writeDX as _writeDX_
from .MIE import load2disk as _load2disk_
import MDAnalysis as _MDAnalysis_

_command_ = 'entropy_analysis'

class Analysis:
    
    def __init__(self,
                 mi1                = None,
                 mi2                = None,
                 mi3                = None,
                 pdb_tmp            = None,
                 pdb_out            = None,
                 bulk               = 0.0,
                 index              = None,
                 output             = None,
                 protein_structure  = None,
                 closest            = 0,
                 volmap_out         = None,
                 volmap_bins        = 129,
                 volmap_mode        = 'nearest',
                 pdb_mi_contrib     = None,
                 ):
        
        if str(pdb_tmp) != str(None):
            if pdb_tmp[-4:].lower() != '.pdb':
                raise ValueError('pdb_tmp needs to be a pdb file.')
        
        if str(pdb_out) != str(None):
            if pdb_out[-4:].lower() != '.pdb':
                raise ValueError('pdb_out needs to be a pdb file.')
            if str(pdb_tmp) == str(None):
                raise ValueError('pdb_out requires pdb_tmp.')
                
        # volmap checks
        if str(volmap_out) != str(None):
            if volmap_out[-3:].lower() != '.dx':
                raise ValueError('volmap_out needs to be a dx file.')
            if volmap_mode.lower() not in ['nearest', 'radialbasis'] and not _os_.path.isfile(volmap_mode):
                raise ValueError('volmap_mode needs to be "nearest" or "radialbasis" or a valid trajectory file.')
                
        # pdb_mi_contribution checks
        if str(pdb_mi_contrib) != str(None):
            if not _os_.path.isdir(pdb_mi_contrib):
                raise ValueError('pdb_mi_contrib needs to be a directory.')
            if str(pdb_tmp) == str(None):
                raise ValueError('pdb_mi_contrib requires pdb_tmp.')
        
        self.mi1                = mi1
        self.mi2                = mi2
        self.mi3                = mi3
        self.pdb_tmp            = pdb_tmp
        self.pdb_out            = pdb_out
        self.bulk               = bulk
        self.index              = index
        self.output             = output
        self.protein_structure  = protein_structure
        self.closest            = closest
        self.volmap_out         = volmap_out
        self.volmap_bins        = volmap_bins
        self.volmap_mode        = volmap_mode
        self.pdb_mi_contrib     = pdb_mi_contrib        
        
    def run(self):
        '''
        Runs the analysis using the provided data and returns the total entropy
        as well as the average entropy per molecule.
        '''
                
        if self.bulk == 'trans':
            S_bulk = 72.00 #translation
        elif self.bulk == 'rot':
            S_bulk = 40.00 #rotation
        else:
            S_bulk = float(self.bulk)
            
        #%% read entropy data
        if str(self.mi1) != str(None):
            S1, _ = _load2disk_(1, self.mi1)
        else:
            S1 = []
        if str(self.mi2) != str(None):
            S2, _ = _load2disk_(2, self.mi2)
        else:
            S2 = []
        if str(self.mi3) != str(None):
            S3, _ = _load2disk_(3, self.mi3)
        else: 
            S3 = []
        
        #%% calculate per-particle entropy
        
        # 1st order
        if len(S1) > 0:
            S = _np_.copy(S1)
        else:
            n = len(_np_.unique([x[0] for x in S2]))
            S = _np_.zeros(n)
        
        # 2nd order
        MI2 = _np_.zeros(S.shape)
        for pair, mi in S2:
            i,j = list(pair)
            dS = -mi/2.
            MI2[i] = MI2[i] + dS
            MI2[j] = MI2[j] + dS
            
        # 3nd order
        MI3 = _np_.zeros(S.shape)
        for tripple, mi in S3:
            i,j,k = list(tripple)
            dS = mi/3.
            MI3[i] = MI3[i] + dS
            MI3[j] = MI3[j] + dS
            MI3[k] = MI3[k] + dS
            
        S_tot = S + MI2 + MI3
        
        #%% read protein stuff and calculate distances
        def water_protein_distance(OW_pos, CAs):
            Distances = _np_.zeros(len(OW_pos))
            for i, ow in enumerate(OW_pos):
                Distances[i] = _np_.linalg.norm(OW_pos[i] - CAs, axis=1).min()
            return Distances
        
        if str(self.protein_structure) != str(None):
            CAs, N_CA, _ = _loadGRO_(self.protein_structure)
            CAs = CAs.reshape((N_CA, 3))
            universe = _MDAnalysis_.Universe(self.pdb_tmp,self.pdb_tmp, in_memory=False)
            OW = universe.select_atoms('name OW')
            D = water_protein_distance(OW.positions*0.1, CAs)
        else:
            D = _np_.zeros(len(S_tot))
            
        #%% prepare index
        if str(self.index) != str(None):
            dic = _readNdx_(self.index)
            Index = _np_.array(dic[dic.keys()[0]]) - 1
        else:
            Index = _np_.arange(len(S_tot))
            
        if self.closest:
            Index = D.argsort()[:self.closest]
            Index = _np_.sort(Index)
            
        # do output
        if str(self.pdb_out) != str(None): 
            print 'Writing pdb outputs ...'
            _writeBfactorPDB_(S_tot[Index]-S_bulk, self.pdb_tmp, self.pdb_out, Indices=Index+1)
            if str(self.mi1) != str(None):
                _writeBfactorPDB_(S[Index], self.pdb_tmp, self.pdb_out[:-4]+'_mi1.pdb', Indices=Index+1)
            if str(self.mi2) != str(None):
                _writeBfactorPDB_(MI2[Index], self.pdb_tmp, self.pdb_out[:-4]+'_mi2.pdb', Indices=Index+1)
            if str(self.mi3) != str(None):
                _writeBfactorPDB_(MI3[Index], self.pdb_tmp, self.pdb_out[:-4]+'_mi3.pdb', Indices=Index+1)
            
        if str(self.volmap_out) != str(None):  
            print 'Writing volume maps ...'
            Mask = _writeDX_(S_tot[Index], self.pdb_tmp, self.volmap_out,
                      bins=self.volmap_bins, Indices=Index, interpolation=self.volmap_mode)
            if str(self.mi1) != str(None):
                _writeDX_(S[Index], self.pdb_tmp, self.volmap_out[:-3]+'_mi1.dx', 
                          bins=self.volmap_bins, Indices=Index, interpolation=self.volmap_mode,
                          interpolationMask = Mask)
            if str(self.mi2) != str(None):
                _writeDX_(MI2[Index], self.pdb_tmp, self.volmap_out[:-3]+'_mi2.dx', 
                          bins=self.volmap_bins, Indices=Index, interpolation=self.volmap_mode,
                          interpolationMask = Mask)
            if str(self.mi3) != str(None):
                _writeDX_(MI3[Index], self.pdb_tmp, self.volmap_out[:-3]+'_mi3.dx', 
                          bins=self.volmap_bins, Indices=Index, interpolation=self.volmap_mode,
                          interpolationMask = Mask)
        
        if str(self.output) != str(None):
            print 'Writing output file ...'
            _np_.savetxt(self.output, _np_.array([(Index+1), D[Index], 
                    S_tot[Index]-MI2[Index]-MI3[Index], MI2[Index], MI3[Index], 
                    S_tot[Index]]).T, fmt='%4i  %7.3f  %7.3f  %7.3f  %7.3f  %7.3f',
                    header='sum = %f, average = %f\nIndex  prot_distance [nm]  entropy [J/mol/K] (single, double, triple, total)' %(_np_.sum(S_tot[Index]), _np_.mean(S_tot[Index])) )
        
        if str(self.pdb_mi_contrib) != str(None):
            print 'Writing MI contributions to pdb files.'
            for ndx in xrange(len(S_tot)):
                MI2_temp = _np_.zeros(len(S_tot))
                for (i,j), mi in S2:
                    if ndx == i:
                        #MI2_temp[ndx] = MI2_temp[ndx] -mi/2.
                        MI2_temp[j] =  MI2_temp[j] -mi/2.
                    elif ndx == j:
                        #MI2_temp[ndx] = MI2_temp[ndx] -mi/2.
                        MI2_temp[i] =  MI2_temp[i] -mi/2.
                _writeBfactorPDB_(MI2_temp[Index], self.pdb_tmp, 
                                  '%s/%i_mi2.pdb' %(self.pdb_mi_contrib, ndx), 
                                  Indices=Index+1)
                    
                MI3_temp = _np_.zeros(len(S_tot))
                for (i,j,k), mi in S3:
                    if ndx == i:
                        #MI3_temp[ndx] = MI3_temp[ndx] +mi/3.
                        MI3_temp[j] =  MI3_temp[j] +mi/3.
                        MI3_temp[k] =  MI3_temp[k] +mi/3.
                    elif ndx == j:
                        #MI3_temp[ndx] = MI3_temp[ndx] +mi/3.
                        MI3_temp[i] =  MI3_temp[i] +mi/3.
                        MI3_temp[k] =  MI3_temp[k] +mi/3.
                    elif ndx == k:
                        #MI3_temp[ndx] = MI3_temp[ndx] +mi/3.
                        MI3_temp[i] =  MI3_temp[i] +mi/3.
                        MI3_temp[j] =  MI3_temp[j] +mi/3.
                _writeBfactorPDB_(MI3_temp[Index], self.pdb_tmp, 
                                  '%s/%i_mi3.pdb' %(self.pdb_mi_contrib, ndx), 
                                  Indices=Index+1)
    
        print 'Total entropy: %7.3f J/mol/K, %7.3f J/mol/K per molecule' %(_np_.sum(S_tot[Index]), _np_.mean(S_tot[Index]))
        return _np_.sum(S_tot[Index]), _np_.mean(S_tot[Index])
        
    def parse(self):
        """
        Parse the a command line to run the job.
        """
        command = ( '%s '
                '-mi1 %s '
                '-mi2 %s '
                '-mi3 %s '
                '-pdb_tmp %s '
                '-pdb_out %s '
                '-bulk %s '
                '-n %s '
                '-o %s '
                '-ps %s '
                '-c %i ' 
                '-vm_o %s '
                '-vm_b %i '
                '-vm_m %s '
                '-pdb_mi_contrib %s '
                ) %(
                        _command_,
                        self.mi1,
                        self.mi2,
                        self.mi3,
                        self.pdb_tmp,
                        self.pdb_out,
                        str(self.bulk),
                        self.index,
                        self.output,
                        self.protein_structure,
                        self.closest,
                        self.volmap_out,
                        self.volmap_bins,
                        self.volmap_mode,
                        self.pdb_mi_contrib,
                )
        return command
        
#%% Input parsing
def __main__():
    
    _print_commandline_stuff_()
    
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--mi1', '-mi1', type=str, help='Entropy-file of first order MIE.')
    parser.add_argument('--mi2', '-mi2', type=str, help='Entropy-file of second order MIE.')
    parser.add_argument('--mi3', '-mi3', type=str, default=None, help='Entropy-file of third order MIE.')
    parser.add_argument('--pdb_tmp', '-pdb_tmp', type=str, default=None, help='PDB-template to write entropy change as beta-factor. Number of atoms must agree.')
    parser.add_argument('--pdb_out', '-pdb_out', type=str, default=None, help='PDB-output.')
    parser.add_argument('--bulk', '-bulk', type=str, default='trans', help='Bulk entropy value. Either "trans" or "rot" or number.')
    parser.add_argument('--index', '-n', type=str, default=None, help='Index-file for entropy calculation.')
    parser.add_argument('--output', '-o', type=str, default=None, help='Output file.')
    parser.add_argument('--protein_structure', '-ps', type=str, default=None, help='Protein structure file (gro, pdb).')
    parser.add_argument('--closest', '-c', type=int, default=0, help='Overwrites index and takes the closest x molecules instead. 0 = all.')
    parser.add_argument('--volmap_out', '-vm_o', type=str, default=None, help='Volume map output file (.dx).')
    parser.add_argument('--volmap_bins', '-vm_b', type=str, default=129, help='Number of bins used for the volume map.')
    parser.add_argument('--volmap_mode', '-vm_m', type=str, default='nearest', help='Mode used for volume map (nearest, radialbasis or a permuted trajectory file containing oxygen atoms).')
    parser.add_argument('--pdb_mi_contrib', '-pdb_mi_contrib', type=str, default=None, help='Directory to store MI contributions to each atom in.')    
    args = parser.parse_args()
        
    a = Analysis(
                 mi1                = args.mi1,
                 mi2                = args.mi2,
                 mi3                = args.mi3,
                 pdb_tmp            = args.pdb_tmp,
                 pdb_out            = args.pdb_out,
                 bulk               = args.bulk,
                 index              = args.index,
                 output             = args.output,
                 protein_structure  = args.protein_structure,
                 closest            = args.closest,
                 volmap_out         = args.volmap_out,
                 volmap_bins        = args.volmap_bins,
                 volmap_mode        = args.volmap_mode,
                 pdb_mi_contrib     = args.pdb_mi_contrib,
            )
    a.run()
    
#%% other helpful functions

def water_shell_preselection(
        protein_structure,
        water_structure,
        water_trajectory,
        molecules,
        output_structure,
        output_trajectory,
        output_index = 'shell.ndx',
        atoms_per_molecule = 3,
        ):
    """
    This function selects a shell of 'molecules' many water molecules around the protein.    
    """
        
    CA_file = protein_structure
    water_traj_file = water_trajectory
    water_stru_file = water_structure
    N = molecules
    
    #output
    struc_out = output_structure
    traj_out  = output_trajectory
        
    def water_protein_distance(OW_pos):
        Distances = _np_.zeros(len(OW_pos))
        for i, ow in enumerate(OW_pos):
            Distances[i] = _np_.linalg.norm(OW_pos[i] - 10*CAs, axis=1).min()
        return Distances
    
    def resNdx2Ndx(res_ndx):
        #N_ndx = len(res_ndx)
        #Ndx = _np_.zeros(atoms_per_molecule*N_ndx)
        #for i in xrange(N_ndx):
        #    Ndx[atoms_per_molecule*i:(atoms_per_molecule*i+atoms_per_molecule)] = Residues[res_ndx[i]].indices
        Ndx = _np_.concatenate(Residues[res_ndx].indices)
        return Ndx+1 
    
    # Load CAs
    CAs, N_CA, _ = _loadGRO_(CA_file)
    CAs = CAs.reshape((N_CA, 3))
    
    
    universe = _MDAnalysis_.Universe(water_stru_file,water_traj_file, in_memory=False)
    Residues = universe.residues
    OW = universe.select_atoms('name OW')
    D_max = []
    
    for i, ts in enumerate(universe.trajectory):
        D = water_protein_distance(OW.positions)
        res_ndx = D.argsort()[:N]
        
        d_max = D[res_ndx[-1]]*0.1
        D_max.append(d_max)
        
        if i%100==0:
            print 'Frame: %i, Maximum distance: %4.2fnm' %(i,d_max)
        
        if i==0:
            with _MDAnalysis_.Writer(struc_out) as W:
                W.write(universe.residues[res_ndx])
            ndx = resNdx2Ndx(res_ndx)
            _writeNdx_(ndx, 'shell', str(output_index), append=False, check_double=False)
            if struc_out != None:
                _os_.system('gmx trjconv -f %s -s %s -n %s -o shell_tmp.trr' %(water_traj_file, struc_out, output_index))
            if output_index == None:
                _os_.system('rm %s' %output_index)        
            W = _MDAnalysis_.Writer(traj_out, atoms_per_molecule*N)
        else:
            W.write(universe.residues[res_ndx].atoms)
    
    _os_.system('rm shell_tmp.trr')    
    W.close()
        
    print 'Average layer thickness: %4.2fnm' %_np_.mean(D_max)  
