#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2021  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

Non-parametric computation of entropy and mutual-information
Adapted by G Varoquaux for code created by R Brette, itself
from several papers (see in the code).
These computations rely on nearest-neighbor statistics
"""

import numpy as np
from scipy.special import psi
import nmslib
import scipy
import pkg_resources

from ..mda_basics.constants import kB
from .kNN_trans import _reshape_

__volume_file_euclquat1__ = 'calculate_volumes/euclquat1.dat'
__volume_file_eucl2quat1__ = 'calculate_volumes/eucl2quat1.dat'
__volume_file_eucl1quat2__ = 'calculate_volumes/eucl1quat2.dat'

#initialize euclquat1-volume
def init_euclquat1_volume():
    filename = pkg_resources.resource_filename(__name__, 
                                                __volume_file_euclquat1__)
    nodes, volumes = np.loadtxt(filename, unpack=True)
    return scipy.interpolate.interp1d(nodes,volumes, kind='cubic')
__euclquat1_volume__ = init_euclquat1_volume()

#initialize eucl2quat1-volume
def init_eucl2quat1_volume():
    filename = pkg_resources.resource_filename(__name__, 
                                                __volume_file_eucl2quat1__)
    nodes, volumes = np.loadtxt(filename, unpack=True)
    return scipy.interpolate.interp1d(nodes,volumes, kind='cubic')
__eucl2quat1_volume__ = init_eucl2quat1_volume()

#initialize eucl1quat2-volume
def init_eucl1quat2_volume():
    filename = pkg_resources.resource_filename(__name__, 
                                                __volume_file_eucl2quat1__)
    nodes, volumes = np.loadtxt(filename, unpack=True)
    return scipy.interpolate.interp1d(nodes,volumes, kind='cubic')
__eucl1quat2_volume__ = init_eucl1quat2_volume()

              
def nearest_distances(X, k=1, metric='euclquat1'):
    '''
    X = array(N,M)
    N = number of points
    M = number of dimensions
    returns the distance to the kth nearest neighbor for every point in X
    '''

    index = nmslib.init(method='vptree', space=metric, 
                        data_type = nmslib.DataType.DENSE_VECTOR,
                        dtype = nmslib.DistType.FLOAT) #, space_params= {'bucketSize': 100})
    index.addDataPointBatch(X)
    index.createIndex(print_progress=False)
    #index.setQueryTimeParams({'maxLeavesToVisit': 2147483647})
    neighbors = index.knnQueryBatch(X, k=k, num_threads=1)
    return np.array([n[1][-1] for n in neighbors])

def entropy(X, k=1, metric=None):
    ''' Returns the entropy of the X using kB=1.
    Parameters
    ===========
    X : array-like, shape (n_samples, n_features)
        The data the entropy of which is computed
    k : int, optional
        number of nearest neighbors for density estimation
    Notes
    ======
    Kozachenko, L. F. & Leonenko, N. N. 1987 Sample estimate of entropy
    of a random vector. Probl. Inf. Transm. 23, 95-101.
    See also: Evans, D. 2008 A computationally efficient estimator for
    mutual information, Proc. R. Soc. A 464 (2093), 1203-1215.
    and:
    Kraskov A, Stogbauer H, Grassberger P. (2004). Estimating mutual
    information. Phys Rev E 69(6 Pt 2):066138.
    '''

    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape
                
    #determine metric
    if d == 7:
        metric = 'euclquat1'
    elif d == 10:
        metric = 'eucl2quat1'
    elif d == 11:
        metric = 'eucl1quat2'
    else:
        raise ValueError('The provided data is of dimension %i. Dimension needs to be 7 (trans-rot), 10 (trans-trans-rot), or 11 (trans-rot-rot).' %d)

    # Distance to kth nearest neighbor
    r = nearest_distances(X, k + 1, metric=metric) # squared distances, k+1 because in the scope of sklearn, 1th neighbor is the point itself
    
    #calculate volumes
    if metric == 'euclquat1':
        volumes = __euclquat1_volume__(r)
    elif metric == 'eucl2quat1':
        volumes = __eucl2quat1_volume__(r)
    elif metric == 'eucl1quat2':
        volumes = __eucl1quat2_volume__(r)
        
    '''
    F. Perez-Cruz, (2008). Estimation of Information Theoretic Measures
    for Continuous Random Variables. Advances in Neural Information
    Processing Systems 21 (NIPS). Vancouver (Canada), December.
    return d*mean(log(r))+log(volume_unit_ball)+log(n-1)-log(k)
    '''
    
    return np.mean(np.log(volumes)) + psi(n) - psi(k)
    #psi(k) instead of log(k) due to correction term, see e.g. "Efficient Estimation of Mututal Information for
    #Strongly Dependent Variables" by Shuzang Gao.

def calcEnt(Ti, k=100, metric=None):
    """
    Computes the momentum-, configurational- and total entropy in physical units.
    
    Parameters
    ----------
    Ti : array-like (n_samples, n_features)
         Trajectory of which entropy is computed.
    k  : int, optional
         k-Value of kNN-algorithm. Default: 100.
         
    Returns
    -------
    S  : 3-tuple
         S-momentum, S-conformation, S-total
    """
    
    shape = Ti.shape
    if len(shape) == 1:
        dof = 1.
    else:
        _,dof = shape
    
    S_conf = kB * entropy(Ti, k=k, metric=metric) - kB*np.log(2) #last term accounts for the 2fold symmetry of water

    return 0, S_conf, S_conf

def calcMI(TrajT, TrajR, i, j, k=100, verbouse=False):
    """
    Computes the mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    TrajT: array-like (n_samples, 3)
           Translational trajectory.
    TrajR: array-like (n_samples, 3)
           Rotational trajectory.
    i    : int
           index of first marginal
    j    : int
           index of second marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100. 
    verbouse: 
           Print warnings, if MI < 0.

    Returns
    -------
    MI : float
         mutual information
    """   
                    
    Ti = _reshape_(TrajT[:,i])[0]
    Tj = _reshape_(TrajR[:,j])[0] 
        
    Sij = calcEnt(np.hstack([Ti,Tj]),k = k)[2]           
    SiSj = calcEnt(np.hstack([Ti,np.random.permutation(Tj)]), k = k)[2]
    MI = SiSj-Sij
    
    if MI <= 0 and verbouse:
        print "Found MI <= 0 (%f) for i=%4i, j=%4i." %(MI,i,j)
    
    return MI

def calcMI21(TrajT, TrajR, l,m,n, k=100, verbouse=False, metric=None):
    """
    Computes the tripple mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    l    : int
           index of first marginal
    m    : int
           index of second marginal
    n    : int
           index of third marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.

    Returns
    -------
    MI : float
         mutual information
    """    
        
    Tl = _reshape_(TrajT[:,l])[0]
    Tm = _reshape_(TrajT[:,m])[0]
    Tn = _reshape_(TrajR[:,n])[0]
        
    S_llmmnn = calcEnt(np.hstack([Tl,Tm,Tn]),k=k, metric = metric)[2]
    S_lmn    = calcEnt(np.hstack([np.random.permutation(Tl),np.random.permutation(Tm),np.random.permutation(Tn)]),
                       k = k, metric = metric)[2]
    S_llmmn  = calcEnt(np.hstack([Tl,Tm,np.random.permutation(Tn)]),k=k,
                       metric = metric)[2]
    S_llmnn  = calcEnt(np.hstack([Tl,np.random.permutation(Tm),Tn]),k=k,
                       metric = metric)[2]
    S_lmmnn  = calcEnt(np.hstack([np.random.permutation(Tl),Tm,Tn]),k=k,
                       metric = metric)[2]
    
    MI = 2.*S_lmn -S_llmmn -S_llmnn -S_lmmnn +S_llmmnn
        
    return MI

def calcMI12(TrajT, TrajR, l,m,n, k=100, verbouse=False, metric=None):
    """
    Computes the tripple mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    l    : int
           index of first marginal
    m    : int
           index of second marginal
    n    : int
           index of third marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.

    Returns
    -------
    MI : float
         mutual information
    """    
        
    Tl = _reshape_(TrajT[:,l])[0]
    Tm = _reshape_(TrajR[:,m])[0]
    Tn = _reshape_(TrajR[:,n])[0]
        
    S_llmmnn = calcEnt(np.hstack([Tl,Tm,Tn]),k=k, metric = metric)[2]
    S_lmn    = calcEnt(np.hstack([np.random.permutation(Tl),np.random.permutation(Tm),np.random.permutation(Tn)]),
                       k = k, metric = metric)[2]
    S_llmmn  = calcEnt(np.hstack([Tl,Tm,np.random.permutation(Tn)]),k=k,
                       metric = metric)[2]
    S_llmnn  = calcEnt(np.hstack([Tl,np.random.permutation(Tm),Tn]),k=k,
                       metric = metric)[2]
    S_lmmnn  = calcEnt(np.hstack([np.random.permutation(Tl),Tm,Tn]),k=k,
                       metric = metric)[2]
    
    MI = 2.*S_lmn -S_llmmn -S_llmnn -S_lmmnn +S_llmmnn
        
    return MI

from .kNN_rot import createUniformSample as _createUniformSample_rot_
def createUniformSample(Nframes, width = 1, maxdistance = np.pi):
    """
    Returns a sample of euclquats. The euclidean part is gaussian-distrubuted
    with a standard deviation of width. Quaternions have a maximal distance 
    of maxdistance to the neutral element (1,0,0,0).
    This function is supposed to be used for testing.
    
    Parameters
    ----------
    Nframes    : Number of frames.
    width      : Standard deviation of the gaussian random numbers.
    maxdistance: Maximum distance to the neutral element (1,0,0,0).
    
    Returns
    -------
    A numpy-array containing the sample, the true entropy of the sample
    in natural units.
    """  
    
    TrajT = np.random.normal(scale=width, size=(Nframes,3))
    TrajR, _ = _createUniformSample_rot_(Nframes, maxdistance = maxdistance)
    Traj  = np.hstack([TrajT, TrajR])

    def uniformSampleEntropy(width = 1, maxdistance = np.pi):
        s_t = 1.5*np.log(2*np.pi*np.e*width*width)
        s_r = np.log(8*np.pi*(maxdistance - np.sin(maxdistance)))
        return s_t + s_r

    return Traj, uniformSampleEntropy(width = width, maxdistance = maxdistance)
