#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 19:55:36 2017

@author: lheinz
"""

import numpy as np

waterfile = 'water.gro'
masses = (16., 1., 1.) #in amu
mode = 'O' #either O or COM

#read file
Coords = []
f = open(waterfile,'r')
f.readline()
f.readline()
for line in f:
    if 'SOL' not in line:
        continue
    coords = map(float, line.strip().split()[3:])
    Coords.append(coords)
f.close()
Coords = np.array(Coords)

#transform to COM coordinates
COM = (Coords[0]*masses[0] + Coords[1]*masses[1] + Coords[2]*masses[2])/np.sum(masses)

if mode == 'COM':
    Coords = Coords - COM
elif mode == 'O':
    Coords = Coords - Coords[0]

#build tensor
I = np.zeros((3,3))
for i in xrange(3):
    x,y,z = Coords[i]
    I[0,0] = I[0,0] + masses[i] * (y*y+z*z)
    I[1,1] = I[1,1] + masses[i] * (x*x+z*z)
    I[2,2] = I[2,2] + masses[i] * (y*y+x*x)
    I[0,1] = I[0,1] - masses[i] * x*y
    I[0,2] = I[0,2] - masses[i] * x*z
    I[1,2] = I[1,2] - masses[i] * y*z

I = I + I.T - np.diag(np.diag(I))

#diagonalize
V = np.linalg.eigvals(I)

#save
np.save('water_inertia_%s' %mode,V)