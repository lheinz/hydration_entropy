#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2020  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

Non-parametric computation of entropy and mutual-information
Adapted by G Varoquaux for code created by R Brette, itself
from several papers (see in the code).
These computations rely on nearest-neighbor statistics
"""

import numpy as np
from scipy.special import psi
import nmslib
import scipy
import pkg_resources

from ..mda_basics.constants import kB,hbar,nano,amu,kTJ
from .kNN_trans import _reshape_

__volume_file_quaternion2__ = 'calculate_volumes/quaternion2.dat'
__volume_file_quaternion3__ = 'calculate_volumes/quaternion3.dat'
__water_inertia_COM_file__ = 'calculate_water_inertia/water_inertia_COM.npy'
__water_inertia_O_file__   = 'calculate_water_inertia/water_inertia_O.npy'
__water_inertia_COM__ = np.load(pkg_resources.resource_filename(__name__,
                                __water_inertia_COM_file__)) * amu * nano**2 #moments of inertia
__water_inertia_O__   = np.load(pkg_resources.resource_filename(__name__,
                                __water_inertia_O_file__)) * amu * nano**2 #moments of inertia

#initialize quaternion2-volume
def init_quaternion2_volume():
    filename = pkg_resources.resource_filename(__name__, 
                                                __volume_file_quaternion2__)
    nodes, volumes = np.loadtxt(filename, unpack=True)
    return scipy.interpolate.interp1d(nodes,volumes, kind='cubic')
__quaternion2_volume__ = init_quaternion2_volume()

#initialize quaternion3-volume
def init_quaternion3_volume():
    filename = pkg_resources.resource_filename(__name__, 
                                                __volume_file_quaternion3__)
    nodes, volumes = np.loadtxt(filename, unpack=True)
    return scipy.interpolate.interp1d(nodes,volumes, kind='cubic')
__quaternion3_volume__ = init_quaternion3_volume()

              
def nearest_distances(X, k=1, metric='quaternion1'):
    '''
    X = array(N,M)
    N = number of points
    M = number of dimensions
    returns the distance to the kth nearest neighbor for every point in X
    '''
    if metric == 'quaternion1':
        disttype = nmslib.DistType.DOUBLE
    else:
        disttype = nmslib.DistType.FLOAT
    
    index = nmslib.init(method='vptree', space=metric, 
                        data_type = nmslib.DataType.DENSE_VECTOR,
                        # dtype = nmslib.DistType.FLOAT) #, space_params= {'bucketSize': 100})
                        dtype = disttype)
    index.addDataPointBatch(X)
    index.createIndex(print_progress=False)
    #index.setQueryTimeParams({'maxLeavesToVisit': 2147483647})
    neighbors = index.knnQueryBatch(X, k=k, num_threads=1)
    return np.array([n[1][-1] for n in neighbors])

def __quaternion1_volume__(r):
    rgeo = 2*np.arccos(1 - r**2/2.)
    volumes = 8*np.pi* (rgeo - np.sin(rgeo))
    return volumes

def __quaternion2max_volume__(r):
    rgeo = 2*np.arccos(1 - r**2/2.)
    volumes = ( 8*np.pi* (rgeo - np.sin(rgeo)) )**2
    return volumes

__metrics__ = {
        'quaternion1':    ( 4, __quaternion1_volume__),
        'quaternion2':    ( 8, __quaternion2_volume__),
        'quaternion3':    (12, __quaternion3_volume__),
        'quaternion2max': ( 8, __quaternion2max_volume__),
        }

def entropy(X, k=1, metric=None):
    ''' Returns the entropy of the X using kB=1.
    Parameters
    ===========
    X : array-like, shape (n_samples, n_features)
        The data the entropy of which is computed
    k : int, optional
        number of nearest neighbors for density estimation
    metric: metric to be used, optional - if not given, the canonical
            quaternion metrics will be selected, based on data dimensionality.
    Notes
    ======
    Kozachenko, L. F. & Leonenko, N. N. 1987 Sample estimate of entropy
    of a random vector. Probl. Inf. Transm. 23, 95-101.
    See also: Evans, D. 2008 A computationally efficient estimator for
    mutual information, Proc. R. Soc. A 464 (2093), 1203-1215.
    and:
    Kraskov A, Stogbauer H, Grassberger P. (2004). Estimating mutual
    information. Phys Rev E 69(6 Pt 2):066138.
    '''

    shape = X.shape
    if len(shape) == 1:
        X = X.reshape((-1,1))
        n, d = X.shape
    else:
        n, d = shape
                
    #determine metric
    if metric == None:
        if d == 4:
            metric = 'quaternion1'
        elif d == 8:
            metric = 'quaternion2'
        elif d == 12:
            metric = 'quaternion3'
        else:
            raise ValueError('The provided data is of dimension %i. Dimension needs to be 4, 8 or 12.' %d)
    else:
        try:
            if not __metrics__[metric][0] == d:
                raise ValueError('Selected metric %s is not designed to work in %i dimensions.' %(metric, d))
        except KeyError:
            raise ValueError('Selected metric %s is not designed to work in %i dimensions.' %(metric, d))

    # Distance to kth nearest neighbor
    # k+1 because 1th neighbor is the point itself
    r = nearest_distances(X, k + 1, metric=metric).astype(np.double)
    # print 'minr: %f, maxr: %f' %(r.min(), r.max())
        
    #calculate volumes
    volumes = __metrics__[metric][1](r)
        
    '''
    F. Perez-Cruz, (2008). Estimation of Information Theoretic Measures
    for Continuous Random Variables. Advances in Neural Information
    Processing Systems 21 (NIPS). Vancouver (Canada), December.
    return d*mean(log(r))+log(volume_unit_ball)+log(n-1)-log(k)
    '''
    
    return np.mean(np.log(volumes)) + psi(n) - psi(k)
    #psi(k) instead of log(k) due to correction term, see e.g. "Efficient Estimation of Mututal Information for
    #Strongly Dependent Variables" by Shuzang Gao.

def calcEnt(Ti, I = 'O', k=100, temp=300, metric=None):
    """
    Computes the momentum-, configurational- and total entropy in physical units.
    
    Parameters
    ----------
    Ti : array-like (n_samples, n_features)
         Trajectory of which entropy is computed.
    I  : array of length 3 or str, optional
         Moment of inertia of molecule in SI units. As string, either "COM" or
         "O" depending on the pivot point used for water analysis. Dafault: "O".
    k  : int, optional
         k-Value of kNN-algorithm. Default: 100.
    metric: str, optional
        Metric to be used. If not given, function uses quaternion metric
        in appropriate dimensionality.
         
    Returns
    -------
    S  : 3-tuple
         S-momentum, S-conformation, S-total
    """
    if I == 'O':
        I = __water_inertia_O__
    elif I == 'COM':
        I = __water_inertia_COM__
    
    shape = Ti.shape
    if len(shape) == 1:
        dof = 1.
    else:
        _,dof = shape
    
    if dof == 4:
        S_mom = 3./2*kB* (1 + np.log( 1./(2*np.pi*hbar)**2 * np.product( (2*np.pi*kTJ*temp/300.*I)**(1./3) ) ))
    else:
        S_mom = 0 # S_mom irrelevant when computing mutual information terms
    
    S_conf = kB * entropy(Ti, k=k, metric=metric) - kB*np.log(2) #last term accounts for the 2fold symmetry of water

    return S_mom, S_conf, S_mom+S_conf

def calcMI(Traj, i, j, k=100, verbouse=False, metric=None):
    """
    Computes the mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    i    : int
           index of first marginal
    j    : int
           index of second marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.

    Returns
    -------
    MI : float
         mutual information
    """   
                    
    Ti = _reshape_(Traj[:,i])[0]
    Tj = _reshape_(Traj[:,j])[0] 
        
    Sij = calcEnt(np.hstack([Ti,Tj]),k = k, metric = metric)[2]           
    SiSj = calcEnt(np.hstack([Ti,np.random.permutation(Tj)]), k = k,
                   metric = metric)[2]
    MI = SiSj-Sij
    
    if MI <= 0 and verbouse:
        print "Found MI <= 0 (%f) for i=%4i, j=%4i." %(MI,i,j)
    
    return MI

def calcMI3(Traj,l,m,n,k=100, verbouse=False, metric=None):
    """
    Computes the tripple mutual information of a trajectory in physical units.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    l    : int
           index of first marginal
    m    : int
           index of second marginal
    n    : int
           index of third marginal
    k    : int, optional
           k-Value of kNN-algorithm. Default: 100.

    Returns
    -------
    MI : float
         mutual information
    """    
        
    Tl = _reshape_(Traj[:,l])[0]
    Tm = _reshape_(Traj[:,m])[0]
    Tn = _reshape_(Traj[:,n])[0]
        
    S_llmmnn = calcEnt(np.hstack([Tl,Tm,Tn]),k=k, metric = metric)[2]
    S_lmn    = calcEnt(np.hstack([np.random.permutation(Tl),np.random.permutation(Tm),np.random.permutation(Tn)]),
                       k = k, metric = metric)[2]
    S_llmmn  = calcEnt(np.hstack([Tl,Tm,np.random.permutation(Tn)]),k=k,
                       metric = metric)[2]
    S_llmnn  = calcEnt(np.hstack([Tl,np.random.permutation(Tm),Tn]),k=k,
                       metric = metric)[2]
    S_lmmnn  = calcEnt(np.hstack([np.random.permutation(Tl),Tm,Tn]),k=k,
                       metric = metric)[2]
    
    MI = 2.*S_lmn -S_llmmn -S_llmnn -S_lmmnn +S_llmmnn
        
    return MI

def createUniformSample(Nframes, maxdistance=np.pi):
    """
    Returns a sample of uniform quaternions which have a maximal distance to the neutral element (1,0,0,0).
    This function is supposed to be used for testing.
    
    Parameters
    ----------
    Nframes    : Number of frames.
    maxdistance: Maximum distance to the neutral element (1,0,0,0).
    
    Returns
    -------
    A numpy-array containing the sample, the true entropy of the sample.    
    """  
    
    def drawRandomRotation(dim=4):
        x = np.random.normal(size=dim)
        return x/np.linalg.norm(x)

    def uniformSampleEntropy(maxdistance=np.pi):
        return np.log(8*np.pi*(maxdistance - np.sin(maxdistance)))

    Traj = np.zeros((Nframes,4))
    for t in xrange(Nframes):
        ok = False
        while not ok:
            x = drawRandomRotation()
            dist = 2*np.arccos(np.abs(x[0]))
            if dist < maxdistance:
                ok = True
        Traj[t] = x
    return Traj, uniformSampleEntropy(maxdistance)
