(* ::Package:: *)

(* ::Title:: *)
(*Calculating eucl2quat1 volumina*)


CloseKernels[];
LaunchKernels[8];


tcos[x_] = Normal[Series[Cos[x],{x,0,12}]];
integrateRegion[RR_] := NIntegrate[2^9*\[Pi]^3 * Rx^2*Ry^2 * Sin[p]^2, {p,Rx,Ry} \[Element] RR, AccuracyGoal->7, PrecisionGoal->7, WorkingPrecision->15]; 

integrateRadius[rr_] := Module[ {result}, 
                                result = integrateRegion[ImplicitRegion[Rx^2 + Ry^2 + 2*(1-tcos[p]) < rr^2, {{p,0,\[Pi]/2},{Rx,0,\[Infinity]},{Ry,0,\[Infinity]}}]];
                                PrintTemporary[rr];
                                Return[result];
                                ];

nodes = 15000;
radiusTable = N[Array[# &, nodes, {10^-4, 15.0 }], 100];


volumina = ParallelMap[integrateRadius, radiusTable];


SetDirectory[DirectoryName[$InputFileName]];
headers = {"#radius", "#volume -- cos taylor order: 12, AccuracyGoal: 7, PrecisionGoal: 7, WorkingPrecision: 15"};
data = Transpose[{radiusTable, volumina}];
dataH = Prepend[data, headers];
Export["eucl2quat1_mathematica_A.dat", 
    dataH /. a_?NumberQ :>
	ToString@ScientificForm[N[a,100], 10, ExponentFunction->(#&), NumberFormat->(Row[{#1, "e", #3}] & )]
];


CloseKernels[];
