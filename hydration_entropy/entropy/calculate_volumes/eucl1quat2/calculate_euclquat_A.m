(* ::Package:: *)

(* ::Title:: *)
(*Calculating eucl2quat1 volumina*)


CloseKernels[];
LaunchKernels[8];


tcos[x_] = Normal[Series[Cos[x],{x,0,12}]];
integrateRegion[RR_] := NIntegrate[2^12*\[Pi]^3 * R^2 * Sin[p]^2 * Sin[q]^2, {p,q,R} \[Element] RR, AccuracyGoal->7, PrecisionGoal->7, WorkingPrecision->15]; 

integrateRadius[rr_] := Module[ {result}, 
                                result = integrateRegion[ImplicitRegion[R^2 + 2*(1-tcos[p]) + 2*(1-tcos[q]) < rr^2, {{p,0,\[Pi]/2},{q,0,\[Pi]/2},{R,0,\[Infinity]}}]];
                                PrintTemporary[rr];
                                Return[result];
                                ];

nodes = 5000;
radiusTable = N[Array[# &, nodes, {10^-4, 5.0 }], 100];


volumina = ParallelMap[integrateRadius, radiusTable];


SetDirectory[DirectoryName[$InputFileName]];
headers = {"#radius", "#volume -- cos taylor order: 12, AccuracyGoal: 7, PrecisionGoal: 7, WorkingPrecision: 15"};
data = Transpose[{radiusTable, volumina}];
dataH = Prepend[data, headers];
Export["eucl1quat2_mathematica_A.dat", 
    dataH /. a_?NumberQ :>
	ToString@ScientificForm[N[a,100], 10, ExponentFunction->(#&), NumberFormat->(Row[{#1, "e", #3}] & )]
];


CloseKernels[];
