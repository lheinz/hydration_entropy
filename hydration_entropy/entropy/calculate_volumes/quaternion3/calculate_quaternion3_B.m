(* ::Package:: *)

(* ::Title:: *)
(*Calculating quaternion3 volumina*)


CloseKernels[];
LaunchKernels[24];


tcos[x_] = Normal[Series[Cos[x],{x,0,10}]];
integrateRegion[RR_] := NIntegrate[32768*\[Pi]^3 * Sin[x]^2*Sin[y]^2*Sin[z]^2, {x,y,z} \[Element] RR, AccuracyGoal->5, PrecisionGoal->5, WorkingPrecision->10]; 

integrateRadius[rr_] := Module[ {result}, 
                                result = integrateRegion[ImplicitRegion[3-tcos[x]-tcos[y]-tcos[z]<rr^2/2, {{x,0,\[Pi]/2},{y,0,\[Pi]/2}, {z,0,\[Pi]/2}}]];
                                PrintTemporary[rr];
                                Return[result];
                                ];

nodes = 5000;
radiusTable = N[Array[# &, nodes, {10^-4 + Sqrt[6]/2, Sqrt[6] }], 100];


volumina = ParallelMap[integrateRadius, radiusTable];


SetDirectory[DirectoryName[$InputFileName]];
headers = {"#radius", "#volume -- cos taylor order: 10, AccuracyGoal: 5, PrecisionGoal: 5, WorkingPrecision: 10"};
data = Transpose[{radiusTable, volumina}];
dataH = Prepend[data, headers];
Export["quaternion3_mathematica_B.dat", 
    dataH /. a_?NumberQ :>
	ToString@ScientificForm[N[a,100], 10, ExponentFunction->(#&), NumberFormat->(Row[{#1, "e", #3}] & )]
];


CloseKernels[];
