#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Per|Mut
Copyright (C) 2018-2021  Leonard P Heinz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*********************************************************************

Basic tools for MD-analysis.
"""

import MDAnalysis
import numpy as np
import sys
import os
import itertools
from scipy.stats.mstats import gmean
import constants as c

#%% Load a trajectory
def loadTraj(topFile,trajFile):
    """
    This reads a structure and a trajectory file and returns 
    the trajectory as a numpy-array.
    
    Alternatively, the function reads numpy files (.npz, .npy) which 
    were previously saved using the transform-coordinates tool.
    
    Inputs
    ----------
    topFile:    Structure file (.npz, .npy, .gro, .tpr, .pdb, etc.)
                str
    trajFile:   Trajectory file (.npy, .npz, .trr, .xtc, etc.)
                str

    Returns
    -------
    trajectory (np-array), number of particles (int), number of frames (int),
    and box-sizes (np-array)
    
    or if supplied with numpy files
    translational trajectory (np-array), rotational trajectory (np-array),
    number of particles (int), number of frames (int), box-sizes (np-array),
    average box size (np-array)
    
    """    
    if topFile[-4:] not in ['.npz', '.npy']:
        universe = MDAnalysis.Universe(topFile,trajFile, in_memory=False)
        N = universe.trajectory.n_atoms
        Nframes = universe.trajectory[-1].frame + 1
        
                                     
        #load traj into array                             
        Traj = np.empty((Nframes,3*N),dtype=np.float32)
        Box  = np.empty((Nframes, 3) ,dtype=np.float32)
        for i in xrange(Nframes):
            Traj[i] = np.concatenate(universe.trajectory[i]._pos) * 0.1 #trajectory in nm
            Box[i]  = universe.trajectory[i].dimensions[0:3] * 0.1 #box dimensions in nm
        return Traj, N, Nframes, Box
    else:
        D = np.load(topFile, allow_pickle=True)
        N = int(D['N'])
        Nframes = int(D['Nframes'])
        TrajT = D['Trans'].reshape((Nframes,3*N))
        if len(D['Rot'].shape) != 0:  # only reshape if TrajR is actually there
            TrajR = D['Rot'].reshape((Nframes,4*N))
        else:
            TrajR = None
        Box   = D['Box']
        box_av= D['box_average']
        return TrajT, TrajR, N, Nframes, Box, box_av
    
def loadBox(topFile,trajFile):
    """
    Same as loadTraj but returns N, Nframes and box only.
    
    Inputs
    ----------
    topFile:    Structure file (.npz, .npy, .gro, .tpr, .pdb, etc.)
                str
    trajFile:   Trajectory file (.npy, .npz, .trr, .xtc, etc.)
                str

    Returns
    -------
    number of particles (int), number of frames (int),
    and box-sizes (np-array)
    
    or if supplied with numpy files
    number of particles (int), number of frames (int), box-sizes (np-array),
    average box size (np-array)
    
    """    
    if topFile[-4:] not in ['.npz', '.npy']:
        universe = MDAnalysis.Universe(topFile,trajFile, in_memory=False)
        N = universe.trajectory.n_atoms
        Nframes = universe.trajectory[-1].frame + 1
        
                                                                 
        Box  = np.empty((Nframes, 3) ,dtype=np.float32)
        for i in xrange(Nframes):
            Box[i]  = universe.trajectory[i].dimensions[0:3] * 0.1 #box dimensions in nm
        return N, Nframes, Box
    else:
        D = np.load(topFile)
        N = int(D['N'])
        Nframes = int(D['Nframes'])
        Box   = D['Box']
        box_av= D['box_average']
        return N, Nframes, Box, box_av
    
def loadGRO(groFile):
    """
    Loads a structure file (.gro, .pdb, .tpr) and returns atom coordinates
    as numpy array.
    
    Inputs
    ----------
    groFile:    Structure file (.gro, .tpr, .pdb, etc.)
                str

    Returns
    -------
    Numpy (Nframes, 3N)-shaped array of coordinates, number of particles (int),
    box-dimensions in nm (array of length 3).    
    """  
    universe = MDAnalysis.Universe(groFile,groFile, in_memory=False)
    N = universe.trajectory.n_atoms
                                   
    #load traj into array                             
    Traj = np.empty(3*N,dtype=np.float32)
    Traj = np.concatenate(universe.trajectory[0]._pos) * 0.1 #box dimensions in nm
    Box = universe.trajectory[0].dimensions[0:3] * 0.1    
    return Traj, N, Box
    
# Write trajectory
def writeTraj(trajFile_out, universe, Traj_new, N):
    if len(Traj_new.shape) == 2:
        Nframes, _ = Traj_new.shape
        Traj_new = Traj_new.reshape((Nframes,N,3))
    with MDAnalysis.Writer(trajFile_out, N) as W:
        if len(Traj_new) > 1:
            i = 0
            for ts in universe.trajectory:
                universe.coord._pos = 10* Traj_new[i]
                i = i + 1
                W.write(universe.coord)
        else:
            universe.coord.positions = 10* Traj_new[0]
            W.write(universe)

#%% Perform PCA
def anaeig(Traj,m=40., indist=True):
    Nframes, N = Traj.shape
    N = N/3.
    C = np.cov(Traj.T)
    eigenval, eigenvec = np.linalg.eig(C)
    ndx = np.argsort(eigenval)[::-1]
    eigenval = eigenval[ndx]
    eigenvec = eigenvec.T[ndx].T
    
    #project onto eigenvectors
    ev1, ev2 = eigenvec.T[0:2]
    Traj_new = np.dot(Traj,eigenvec)
    
    #calculate entropy using QH
    SQH = c.kB * np.sum( 0.5* np.log( c.uNmkTOverHbar2 * m * eigenval) + 1) #absolute entropy
    #gamma = kTJ/(eigenval*nano*nano)
    #SQHconf = 0.5 * k * np.sum( np.log(2*np.pi * kTJ/(gamma*hx*hx)) + 1)
    
    #calculate entropy using Schlitter
    SS = 0.5 * c.kB * np.sum( np.log(1 + c.uNmkTOverHbar2 * np.exp(2) * m * eigenval)) #absolute entropy
    
    #calculate gaussian entropy
    Sgau = 0.5*c.kB * (3*N + 3*N*np.log(2*np.pi) + np.sum(np.log(eigenval)))
    
    #apply currection for indistinguishable particles
    if indist:
        corr = c.kB * (N * np.log(N) - N)
        SQH = SQH - corr
        #SQHconf = SQHconf - corr
        SS = SS - corr
        Sgau = Sgau - corr
        
    return (Traj_new,eigenval,eigenvec), (SQH,SS,Sgau)

def whiten(Traj,eigenval,conserve_entropy=False):
    if conserve_entropy:
        scale = gmean(np.sqrt(eigenval))
    else:
        scale = 1.
    return np.dot(Traj - np.mean(Traj,axis=0),scale*np.diag(1./np.sqrt(eigenval)))

def unwhiten(Traj,eigenval,conserve_entropy=False):
    if conserve_entropy:
        scale = gmean(np.sqrt(eigenval))
    else:
        scale = 1.
    return np.dot(Traj - np.mean(Traj,axis=0),1./scale*np.diag(np.sqrt(eigenval)))

#%% autocorrelation
def autocorrelation(x):
    n = len(x)
    variance = x.var()
    x = x-x.mean()
    r = np.correlate(x, x, mode = 'full')[-n:]
    #assert np.allclose(r, np.array([(x[:n-k]*x[-(n-k):]).sum() for k in range(n)]))
    result = r/(variance*(np.arange(n, 0, -1)))
    return result

#%% calculate the distances between lists, taking PBCs into account
def PBCdistances(X,Y,L):
    """
    Takes two phase space vectors and calculates distances while accounting for PBCs.
    
    Parameters
    ----------
    X    : 1D array-like
           First vector.
    Y    : 1D array-like
           Second vector.
    L    : float or array like with length 3.
           Dimensions of the simulation box.

    Returns
    -------
    D    : numpy-array of distances
    """    
    n = len(X)
    
    if n != len(Y) or n/3. != n/3:
        raise ValueError('X and Y need to be of identical length, which needs to be a multiple of 3.')
            
    if type(L) in [int, float]:
        L = np.array(n*[L])
    else:
        if 3 == len(L):
            L = np.array(n/3*list(L))
        else:
            raise ValueError('L needs to be either a number of an array of lenth 3.')
            
    D = np.abs(X - Y)
    Dbar = L - D
    return np.min([D,Dbar],axis=0)

#%%
def particleNdx2dof(index):
    """
    Takes a particle index and returns a list of corresponding degrees of freedom.
    
    Parameters
    ----------
    index : int
            particle index

    Returns
    -------
    DOF   : A list of corresponding dofs.
    """     
    return np.arange(3*index,3*index+3)

def particleNdx2dof_rot(index):
    """
    Takes a particle index and returns a list of corresponding degrees of freedom.
    
    Parameters
    ----------
    index : int
            particle index

    Returns
    -------
    DOF   : A list of corresponding dofs.
    """     
    return np.arange(4*index,4*index+4)
    

#%% get close neighbots of permutation-reduced trajectory
def getNeighbors(Traj,
                 cut        = np.inf,
                 partners   = 2,
                 L          = 0.0,
                 dofIndex   = True, 
                 getSelf    = False,
                 symmetrize = False,
                 ):
    """
    Takes a trajectory and finds all neighbors within a given cutoff from each particle's average position.
    
    Parameters
    ----------
    Traj : array-like (n_samples, n_features)
           Trajectory of which entropy is computed.
    cut  : float, optional
           The cutoff-radius in nm in which partners are searched for. Default: 1nm.
    partners: int, optional
           number of partners that are looked for; either 2 or 3. Default: 2.
    L    : float or array like with length 3, optional
           Dimensions of the simulation box.
    dofIndex: bool, optional, default: True
           If true, a list of indices of dofs is returned (ranging up to 3N). If false, particle indices (up to N) are returned.
    getSelf: bool, optinal, default: False
           If true, returns also partners of dofs that belong to the same particle, i.e. includes inter-particle dofs.
    symmetrize: bool. optional, default: False
           Symmetrize the returned list. I.e. if the list of neighbors contains [a,b], it will also contain [b,a].
           This only has effects for partners = 2.

    Returns
    -------
    P  : A list of particle indices of interaction partners. Each element consits of a doublet or a tripplet, depending on 'partners'.
    """  
    if cut <= 0:
        cut = np.inf
    
    Mean = np.mean(Traj,axis=0)
    N = len(Mean)
    
    if N/3 != N/3.:
        raise ValueError('The number of degees of freedom must be divisible by 3.')
        
    P = []
    
    for i in xrange(N/3):
        p = Mean[(3*i):(3*i+3)]
        pN = np.tile(p,N/3)
                
        D = PBCdistances(Mean,pN,L=L)
        D = np.linalg.norm(np.reshape(D,(N/3,3)),axis=1)
        Ind = np.arange(N/3)
        Ind = Ind[(0<D) * (D<cut)] #indices of all particles close to the ith particle
        
        if partners==2:
            for ndx in Ind:
                pair = sorted([i,ndx])
                if pair not in P:
                    P.append(pair)
                    if symmetrize:
                        P.append(pair[::-1])
        elif partners==3:
            for pair in itertools.combinations(Ind,2):
                triple = sorted([i] + list(pair))
                if triple not in P:
                    P.append(triple)
        elif partners==21:  # 2 translational, 1 rotational
            for s in Ind:
                triple = sorted([i, s]) + [s]
                if triple not in P:
                    P.append(triple)
                triple = sorted([i, s]) + [i]
                if triple not in P:
                    P.append(triple)
            for pair in itertools.combinations(Ind,2):
                triple = [i] + list(pair)
                triple = sorted(triple[0:2]) + [triple[2]]
                if triple not in P:
                    P.append(triple)
        elif partners==12:  # 1 translational, 2 rotational
            for s in Ind:
                triple = [s] + sorted([i, s])
                if triple not in P:
                    P.append(triple)
                triple = [i] + sorted([i, s])
                if triple not in P:
                    P.append(triple)
            for pair in itertools.combinations(Ind,2):
                triple = [i] + list(pair)
                triple = [triple[0]] + sorted(triple[1:3])
                if triple not in P:
                    P.append(triple)
                    
    if not dofIndex:                    
        return P
    else:
        DOF = []
        if getSelf:
            [DOF.extend(list(itertools.combinations(par,partners))) for par in map(particleNdx2dof,range(N/3))]
        for p in P:
            dof = map(particleNdx2dof,p)
            dof = [p for p in itertools.product(*dof)]
            DOF = DOF + dof
        return DOF
    
#%%
def getDistance(i,j,Structure,L):
    """
    Takes particle indices i and j and a structure and calculates the distance between the particles.
    
    Parameters
    ----------
    i    : int
           First index.
    j    : int
           Second index.
    Struc: array-like
           structure from which the distance is being calculated
    L    : float or array like with length 3.
           Dimensions of the simulation box.

    Returns
    -------
    d    : distance
    """    

    X = Structure[particleNdx2dof(i)]
    Y = Structure[particleNdx2dof(j)]
    d = np.linalg.norm(PBCdistances(X,Y,L))
        
    return d

#%%

def readNdx(index_file):
    """
    Reads a gromacs index file and returns it as a dictionary.
    
    Parameters
    ----------
    index_file    : str
                    Path to index file.

    Returns
    -------
    Ndx           : Index-dict
    """    
    f = open(index_file,'r')
    text = f.read().strip().replace('\n',' ').split('[')
    f.close()
    
    Ndx = {}
    for grouptxt in text:
        if grouptxt =='':
            continue
        grouptxt = grouptxt.replace(']','').strip().split()
        Ndx[grouptxt[0]] = map(int,grouptxt[1:])
        
    return Ndx

def writeNdx(Ndx, index_name, index_file, append=True, check_double=True):
    """
    Writes a gromacs index file.
    
    Parameters
    ----------
    Ndx           : Array containing the indices.
    index_name    : str
                    Name of set of indices to be added.
    index_file    : str 
                    Path to index file.
    append        : bool (optional, default: True)
                    Append to file. Overwrites file if false.
    check_double  : Check of same group (identical name and elements) already
                    exists and if so, do not write it a second time.
    """
    # if index_file does not end with .ndx, append it
    if index_file[-4:] != '.ndx':
        index_file = index_file + '.ndx'
    
    if check_double:
        if os.path.exists(index_file):
            Ndx_f = readNdx(index_file)
            if index_name in Ndx_f.keys():
                if np.product(Ndx==Ndx_f[index_name]):
                    print 'Index already exists. Do not write anything.'
                    return None
    
    if append:
        f = open(index_file, 'a')
    else:
        f = open(index_file, 'w')
        
    f.write('\n[ %s ]\n' %index_name)
    c = 0
    for ndx in Ndx:
        f.write(' %4i' %ndx)
        c = c + 1
        if c%15==0:
            f.write('\n')
            c = 0
    f.write('\n')

#%%
def writeBfactorPDB(S_tot, pdb_template, pdb_out, Indices=[]):
    """
    Reads a pdb template and adds the values of S_tot as the b-factor.
    
    Parameters
    ----------
    S_tot           : list
                    List of values to be added.
    pdb_template    : str
                    Path to the template pdb-file.
    pdb_out         : str
                    Path to the output pdb file.
    Indices         : list (optional)
                    List of atom indices that correspond to the values in S_tot.
    """    
    
    if len(Indices) == 0:
        Indices = np.arange(len(S_tot))+1
        
    f = open(pdb_template,'r')
    Lines = []
    for line in f:
        if 'ATOM' in line:
            Lines.append(line[0:60])
        else:
            Lines.append(line)
    f.close()
    
    f = open(pdb_out,'w')
    ndx = 1
    add = 0
    for line in Lines:
        if 'ATOM' in line:
            atomNo = int(line.split()[4])
            if ndx in Indices:
                s = S_tot[add]
                add = add + 1
                f.write(line.rstrip()+'%6.2f\n' %s)
            else:
                f.write(line.rstrip()+'%6.2f\n' %0)
            ndx = ndx + 1
        else:
            f.write(line)
    f.close()
    if add != len(S_tot):
        raise ValueError('Warning: Number of added b-factor values (%i) and number of values to be added (%i) is not identical.' %(add, len(S_tot)))
 
#%%
def _DX_traj_interpolation_(X, Y, Z, topFile, trajFile, Indices=[]):
    """
    Provides a interpolation mask to be used in writeDX.
    
    Parameters
    ----------
    X,Y,Z           : Define the bins, arrays
    structure       : Path to structure file
    traj_path       : Path to permuted trajectory file containing only oxygens.
    Indices         : list (optional)
                    List of atom 0-based indices that correspond to the atoms that should be used for interpolation.
    """
    
    Traj, N, Nframes, _ = loadTraj(topFile, trajFile)
    Traj = Traj.reshape((Nframes,N,3))
    
    if len(Indices) > 0:
        Traj = Traj[:,Indices,:]
        N = len(Indices)
    
    x_bins, y_bins, z_bins = len(X), len(Y), len(Z)
    # Mask = np.zeros((N, x_bins, y_bins, z_bins))
    keys = list(itertools.product(*[range(x_bins),range(y_bins),range(z_bins)]))
    Mask = {key: [] for key in keys}
    
    # get bin edges
    dx, dy, dz = X[1]-X[0], Y[1]-Y[0], Z[1]-Z[0]
    Xe, Ye, Ze = X-dx/2., Y-dy/2., Z-dz/2.
    Xe, Ye, Ze = np.append(Xe,Xe[-1]+dx), np.append(Ye,Ye[-1]+dy), np.append(Ze,Ze[-1]+dz)
    
    # fill mask
    for i in xrange(N):
        P = Traj[:,i,:]
        H = np.histogramdd(P, bins=[Xe,Ye,Ze])[0]
        for ndx in np.array(np.nonzero(H)).T:
            Mask[tuple(ndx)].append((i, H[ndx[0],ndx[1],ndx[2]])) 
    return Mask
        
        
def writeDX(S, structure, output, bins = 129, Indices=[], interpolation='radialbasis', interpolationMask=None):
    """
    Writes a .dx-file by interpolating the local entopies between the values
    defined in a given array at the positions specified in a given structure file.
    
    Parameters
    ----------
    S               : list
                    List of values to be interpolated. They must correspond to 
                    the number of atoms in the structure file.
    structure       : str
                    Path to the template structure-file (pdb, gro, etc.).
    output          : str
                    Output .dx file.
    bins            : int of 3-tuple of ints (optional)
                    Defines the number of bins in x, y, and z direction. 
                    Either bins or dx must be given.
    Indices         : list (optional)
                    List of atom 0-based indices that correspond to the values in S.
    interpolation   : str or nd-array
                    Interpolation method: nearest, radialbasis or path to 
                    permuted trajectory of oxygens for trajectory-based 
                    interpolation (optional)
    interpolationMask
                    : A interpolation mask as provided by _DX_traj_interpilation_. If none (default)
                    the function will call _DX_traj_interpolation_, if necessary. Otherwise
                    it uses the mask provided.
    """        
    
    if type(bins) != int or (type(bins) == tuple and len(bins) != 3):
        raise ValueError('bins must either be of type int or a 3-tuple of ints.')
    if interpolation.lower() not in ['nearest', 'radialbasis']:
        if os.path.isfile(interpolation):
            interpolation_path = interpolation
            interpolation = 'traj'
        else:
            raise ValueError('interpolation method must be nearest, radialbasis or a valid path to a trajectory.')
        
    # read structure information and gather information
    Struc, N, Box = loadGRO(structure)
    Struc = Struc.reshape((N,3))
    if len(Indices) > 0:
        Struc = Struc[Indices]
        N = len(Struc)
    if type(bins) == int:
        bins = (bins,bins,bins)
    Dx = Box/np.array(bins)
    NData = np.prod(bins)
    origin = np.array((0,0,0))
    
    if N != len(S):
        raise Exception('Atoms found in structure (%i) file and number of elements in S (%i) do not agree.' %(N, len(S)))
        
    def findClosest(x, Struc):
        """
        Finds index of closest element of Struc to x.
        """
        d2 = np.sum((Struc - x)**2, axis=1)
        return d2.argmin()

    # loop over all grid cells and write to file
    X = np.arange(bins[0]) * Dx[0] + origin[0]
    Y = np.arange(bins[1]) * Dx[1] + origin[1]
    Z = np.arange(bins[2]) * Dx[2] + origin[2]
    
    if interpolation.lower() == 'radialbasis':
        from scipy.interpolate import Rbf
        rbf = Rbf(10*Struc[:,0], 10*Struc[:,1], 10*Struc[:,2], S, function='multiquadric')
    elif interpolation == 'traj':
        if interpolationMask is None:
            Mask = _DX_traj_interpolation_(X, Y, Z, structure, interpolation_path, Indices=Indices)
        else:
            Mask = interpolationMask
    
    # write header stuff to output file
    f = open(output, 'w')
    f.write('# Data from entropy analysis\n')
    f.write('# using structure file %s\n' %structure)
    f.write('object 1 class gridpositions counts %i %i %i\n' %bins)
    f.write('origin %8.6e %8.6e %8.6e\n' %tuple(10*origin))
    f.write('delta %8.6e 0.000000e+00 0.000000e+00\n' %(Dx[0]*10))
    f.write('delta 0.000000e+00 %8.6e 0.000000e+00\n' %(Dx[1]*10))
    f.write('delta 0.000000e+00 0.000000e+00 %8.6e\n' %(Dx[1]*10))
    f.write('object 2 class gridconnections counts %i %i %i\n' %bins)
    f.write('object 3 class array type double rank 0 items %i data follows\n' %NData)
        
    
    i = 0
    for ix, x in enumerate(10*X):
        for iy, y in enumerate(10*Y):
            for iz, z in enumerate(10*Z):
                if interpolation == 'nearest':
                    pos = np.array([x,y,z])
                    ndx = findClosest(pos, Struc*10)
                    v = S[ndx]
                elif interpolation == 'radialbasis':
                    v = float(rbf(x,y,z))
                elif interpolation == 'traj':
                    particles = [elem[0] for elem in Mask[(ix,iy,iz)]]
                    weights = [elem[1] for elem in Mask[(ix,iy,iz)]]
                    try:
                        v = np.average(S[particles], weights=weights)
                    except ZeroDivisionError:
                        v = 0
                    
                f.write('%8.6e ' %v)
                if (i+1)%3 == 0:
                    f.write('\n')
                i = i + 1
    if i%3 != 0:
        f.write('\n')
                
    # write footer stuff
    f.write('attribute "dep" string "positions"\n')
    f.write('object "regular positions regular connections" class field\n')
    f.write('component "positions" value 1\n')
    f.write('component "connections" value 2\n')
    f.write('component "data" value 3\n')
    
    if interpolation == 'traj':
        return Mask
    else:
        return None
       
#%%
def _print_commandline_stuff_():
    from pkg_resources import require
    version = require('hydration_entropy')[0].version
    cpright = (
            '---------------------------- Per|Mut ----------------------------\n'
            '\n'
            'Copyright (C) 2018-2021  Leonard P Heinz\n'
            '\n' 
            'This program is free software: you can redistribute it and/or modify\n'
            'it under the terms of the GNU General Public License as published by\n'
            'the Free Software Foundation, either version 3 of the License, or\n'
            '(at your option) any later version.\n'
            '\n'
            'This program is distributed in the hope that it will be useful,\n'
            'but WITHOUT ANY WARRANTY; without even the implied warranty of\n'
            'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n'
            'GNU General Public License for more details.\n'
            '\n'
            'You should have received a copy of the GNU General Public License\n'
            'along with this program.  If not, see <http://www.gnu.org/licenses/>.\n'
            '\n'
            )
    versionstuff = (
            'Program version: Per|Mut %s\n\n'
            ) %(version)
    
    sys.stdout.write(cpright + versionstuff)
    sys.stdout.flush()
    
