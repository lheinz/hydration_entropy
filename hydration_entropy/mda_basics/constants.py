T = 300 #K
kTJ = 1.3806485e-23 * T #J
hbar = 1.0545718e-34 #Js
amu = 1.660539e-27 #atomic mass unit in kg
nano = 1e-9 #1nm im m
uNmkTOverHbar2 = kTJ/(hbar*hbar) * amu * nano*nano #1nm * 1u * kT/hbar**2
kB = 8.31446 #J/molK