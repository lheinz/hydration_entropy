# Per|Mut: Spatially resolved hydration entropies from atomistic simulations [1,2].

This package uses a mutual information expansion to calculate the hydration entropy from an atomistic computer simulation.

A spatial resolution can be achieved if used together with permutation reduction [3], implemented in [g_permute](https://gitlab.gwdg.de/lheinz/g_permute) [4].

## Prerequisite

* python 2.7 with the following packages
    * numpy
    * scipy
    * pkg_resources
    * os
    * sys
    * setuptools
    * gc
    * multiprocessing
    * [MDAnalysis](https://www.mdanalysis.org/) [5,6] 
* [nmslib](https://gitlab.gwdg.de/lheinz/nmslib_quaternion) [7], modified to include quaternion metrics


## Installation

`python setup.py install`

## Calculating entropies

Hydration entropies are calculated with Per|Mut in the following steps:

1. MD simulation, lasting at least one microsecond. We recommend to save frames every 10ps, so that the water molecules are sufficiently uncorrelated for analysis. 

2. Permutation reduction [3] of the water molecules using g_permute [4].

3. Coordinate transformation. For further analysis, the coordinates need to be transformed so that the translational coordinates of each water molecule are given by the position of the oxygen atom or the center-of-mass, and the orientational coordinates are given by quarternions. This can be done using the class `entropy.transformCoordinates.transform` or the command `transformCoordinates`. 

4. Mutual information expansion with `entropy.MIE.mie`. I.e., 
``` python
mie = entropy.MIE.mie('water.npz', 'rot', 2, # calculate two-body rotational MIs
              cutoff = 1.0,  # only calculate for molecule-pairs within a 1.0nm cutoff.
              threads = 1,  # number of threads
              parts = '1/1',  # split calculation into n parts, then execute part i with 'i/n'
              k = 1, # use a k-nearest-neighbor value of k = 1
              )
mie.run()
```
calculates the second order rotational mutual information terms. The second argument can be 'trans', 'rot', or 'mix', where the former calculates the entropy contribution from the translation-rotation correlation (only second order).

5. Analysis with `entropy.analysis.Analysis` to compile a per-molecule list of entropies from the output of the mutual information expansion. 


## Example usage

See folder `example_usage`. Run with

```bash
cd example_usage
python example.py
```
The results are stored in `example_usage/output`.

## References

[1] Heinz, L. P., & Grubmüller, H. (2019). Computing spatially resolved rotational hydration entropies from atomistic simulations. Journal of Chemical Theory and Computation, 16(1), 108-118.

[2] Heinz, L. P., & Grubmüller, H. (2020). Per|Mut: Spatially resolved hydration entropies from atomistic simulations. Journal of Chemical Theory and Computation, submitted.

[3] Reinhard, F., & Grubmüller, H. (2007). Estimation of absolute solvent and solvation shell entropies via permutation reduction. The Journal of chemical physics, 126(1), 014102.

[4] Reinhard, F., Lange, O. F., Hub, J. S., Haas, J., & Grubmüller, H. (2009). g_permute: Permutation-reduced phase space density compaction. Computer Physics Communications, 180(3), 455-458.

[5] Gowers, R. J., Linke, M., Barnoud, J., Reddy, T. J., Melo, M. N., Seyler, S. L., ... & Beckstein, O. (2016, July). MDAnalysis: a Python package for the rapid analysis of molecular dynamics simulations. In Proceedings of the 15th Python in Science Conference (Vol. 98). Austin, TX: SciPy.

[6] Michaud‐Agrawal, N., Denning, E. J., Woolf, T. B., & Beckstein, O. (2011). MDAnalysis: a toolkit for the analysis of molecular dynamics simulations. Journal of computational chemistry, 32(10), 2319-2327.

[7] Boytsov, L., & Naidan, B. (2013, October). Engineering efficient and effective non-metric space library. In International Conference on Similarity Search and Applications (pp. 280-293). Springer, Berlin, Heidelberg.
